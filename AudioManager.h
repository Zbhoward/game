#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H

#include "GameHeader.h"
#include "AudioResource.h"
#include <string>
#include <vector>

class GameManager;

class AudioManager {
	protected:
		GameManager* game_manager;

	public:
		AudioManager(GameManager* game_manager) { this->game_manager = game_manager; }
		virtual ~AudioManager() { game_manager = NULL; }

		virtual void setVolume(float volume) = 0;
		virtual void pauseAll() = 0;
		virtual void start() = 0;

		virtual void playAudio(const char* audio_name, uint32 num_repeats, bool restart = false) = 0;
		virtual void pauseAudio(const char* audio_name) = 0;

		virtual bool isAudioPlaying(const char* audio_name) = 0;

		virtual void loadResources(std::vector<AudioResource*>* resources) = 0;
		virtual void unloadResources(std::vector<AudioResource*>* resources) = 0;
};

#endif