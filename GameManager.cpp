#include "GameManager.h"
#include "OgreRenderManager.h"
#include "ResourceManager.h"
#include "LogManager.h"
#include "OISInputManager.h"
#include "CEGUIManager.h"
#include "BassAudioManager.h"
#include "LuaScriptManager.h"

#include <iostream>

//the problem is that in C++, it is not known when this initialization code will be called
//so we will use a function static variable that is initialized the first time the method is called
//destructor automatically called?
GameManager* GameManager::getGameManager()
{
    static GameManager game_manager;  //initialized when method is called the first time
    return &game_manager;  //won't go out of scope as game_manager is static
}

GameManager::GameManager() {
    log_manager = new LogManager("game.log");
    resource_manager = new ResourceManager("resources.xml", this);
    logComment("Resource Manager initialized");
    render_manager = new OgreRenderManager(this);
    logComment("Render Manager initialized");
    gui_manager = new CEGUIManager(this);
    logComment("GUI Manager initialized");
    input_manager = new OISInputManager(this);
    logComment("Input Manager initialized");
    audio_manager = new BassAudioManager(this);
    logComment("Audio Manager initialized");
    script_manager = new LuaScriptManager(this);
    logComment("Script Manager initialized");

    resource_manager->loadLevel("level_0");
    audio_manager->playAudio("Memories", 1);
    render_manager->startRendering();
}

GameManager::~GameManager()
{
    logComment("GameManager destructor called");
    resource_manager->unloadLevel("level_0");

    delete script_manager;
    script_manager = NULL;
    delete audio_manager;
    audio_manager = NULL;
    delete input_manager;
    input_manager = NULL;
    delete gui_manager;
    gui_manager = NULL;
    delete render_manager;
    render_manager = NULL;
    delete resource_manager;
    resource_manager = NULL;
    delete log_manager;
    log_manager = NULL;
}

RenderManager* GameManager::getRenderManager() {
    return render_manager;
}

GUIManager* GameManager::getGUIManager() {
    return gui_manager;
}

InputManager* GameManager::getInputManager() {
    return input_manager;
}

AudioManager* GameManager::getAudioManager() {
    return audio_manager;
}

ScriptManager* GameManager::getScriptManager() {
    return script_manager;
}

PhysicsManager* GameManager::getPhysicsManager() {
    OgreRenderManager* orm = (OgreRenderManager*) render_manager;
    return orm->getPhysicsManager();
}

void GameManager::loadRenderingResources(const char* level_name, vector<string*>* paths, vector<string*>* meshes) {
    render_manager->loadResources(level_name, paths, meshes);
}

void GameManager::loadAudioResources(vector<AudioResource*>* audio) {
    audio_manager->loadResources(audio);
}

void GameManager::unloadRenderingResources(const char* level_name) {
    render_manager->unloadResources(level_name);
}

void GameManager::unloadAudioResources(vector<AudioResource*>* audio) {
    audio_manager->unloadResources(audio);
}

void GameManager::logProblem(std::string error_msg, std::string file_name, int line_number) {
    log_manager->logProblem(error_msg, file_name, line_number);
}

void GameManager::logComment(std::string comment_message) {
    log_manager->logComment(comment_message);
}

size_t GameManager::getRenderWindowHandle()
{
    return render_manager->getRenderWindowHandle();
}

int GameManager::getRenderWindowWidth()
{
    return render_manager->getRenderWindowWidth();
}

int GameManager::getRenderWindowHeight()
{
    return render_manager->getRenderWindowHeight();
}
