#ifndef RENDER_MANAGER_H
#define RENDER_MANAGER_H

#include <string>
#include <vector>

class GameManager;
struct SceneNodeMotion;
struct SceneNodeManual;

class RenderManager {
	protected:
		enum TransformType { TRANSLATE, ROTATE, SCALE };

		GameManager* game_manager;

	public:
		RenderManager(GameManager* game_manager) { this->game_manager = game_manager; }
		virtual ~RenderManager() { game_manager = NULL; }

		virtual size_t getRenderWindowHandle() = 0;
		virtual int getRenderWindowWidth() = 0;
		virtual int getRenderWindowHeight() = 0;

		virtual void startRendering() = 0;
		virtual void stopRendering() = 0;

		virtual void loadResources(const char* level_name, std::vector<std::string*>* paths, 
			std::vector<std::string*>* meshes) = 0;
        virtual void unloadResources(const char* level_name) = 0;

        virtual void createCamera(float* position, float* look_at, float* clip_distances) = 0;
		virtual void createLight(std::string& name, float* ambient_color, float* diffuse_color, float* direction) = 0;
		virtual void createEntity(std::string& name, std::string& mesh, std::string& material, std::string& scene_node) = 0;
		virtual void createSceneNode(std::string& child, std::string& parent) = 0;
		virtual void createAnimation(std::string& name, float length, std::string& scene_node) = 0;
		virtual void createAnimationKeyFrame(std::string& animation_name, float time) = 0;
		virtual void createAnimationState(std::string& animation_name) = 0;

		virtual void processTranslate(std::string& scene_node, float* values) = 0;
		virtual void processRotate(std::string& scene_node, float* values) = 0;
		virtual void processScale(std::string& scene_node, float* values) = 0;

		virtual void processAnimationTranslate(std::string& animation_name, int frame_index, float* values) = 0;
		virtual void processAnimationRotate(std::string& animation_name, int frame_index, float* values) = 0;
		virtual void processAnimationScale(std::string& animation_name, int frame_index, float* values) = 0;

		virtual void updateAnimations(float time_step) = 0;
		virtual void updateEveryFrame(std::string script) = 0;

		virtual void checkForInput(float time_step) = 0;

		virtual void rotateMesh(std::string node, float value, int x, int y, int z) = 0;
        virtual void startAnimation(std::string animation_name) = 0;

        virtual void drawLine(float* from, float* to, float* color, SceneNodeManual* snm) = 0;

		virtual SceneNodeMotion* createSceneNodeMotion(std::string& scene_node_id) = 0;
		virtual void destroySceneNodeMotion(SceneNodeMotion* snm) = 0;
		virtual SceneNodeManual* createManualObject() = 0;
		virtual void clearManualObject(SceneNodeManual* snm) = 0;
		
		virtual void setPosition(SceneNodeMotion* scene_node_motion, double x, double y, double z) = 0;
		virtual float* getPosition(SceneNodeMotion* scene_node_motion) = 0;
		virtual void setOrientation(SceneNodeMotion* scene_node_motion, double w, double x, double y, double z) = 0;
		virtual float* getOrientation(SceneNodeMotion* scene_node_motion) = 0;
};

#endif