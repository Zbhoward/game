#include "LuaScriptManager.h"
#include "GameManager.h"
#include "AudioManager.h"
#include "RenderManager.h"
#include "PhysicsManager.h"

#include <iostream>

using namespace std;

LuaScriptManager::LuaScriptManager(GameManager* game_manager) : ScriptManager(game_manager) {
	L = luaL_newstate();
	luaL_openlibs(L);

    AudioManager* audio_manager = game_manager->getAudioManager();
    RenderManager* render_manager = game_manager->getRenderManager();
    PhysicsManager* physics_manager = game_manager->getPhysicsManager();
    luabridge::getGlobalNamespace(L)
        .beginClass<AudioManager>("AudioManager")
            .addFunction("setVolume", &AudioManager::setVolume)
            .addFunction("pauseAudio", &AudioManager::pauseAudio)
            .addFunction("isAudioPlaying", &AudioManager::isAudioPlaying)
            .addFunction("playAudio", &AudioManager::playAudio)
        .endClass()
        .beginClass<RenderManager>("RenderManager")
        	.addFunction("rotateMesh", &RenderManager::rotateMesh)
        	.addFunction("startAnimation", &RenderManager::startAnimation)
        .endClass()
        .beginClass<PhysicsManager>("PhysicsManager")
        	.addFunction("applyImpulse", &PhysicsManager::applyImpulse)
        	.addFunction("applyTorqueImpulse", &PhysicsManager::applyTorqueImpulse)
        .endClass();

    luabridge::push(L, audio_manager);
    lua_setglobal(L, "audio_manager");

	luabridge::push(L, render_manager);
    lua_setglobal(L, "render_manager");    

    luabridge::push(L, physics_manager);
    lua_setglobal(L, "physics_manager");
}

LuaScriptManager::~LuaScriptManager() {
	lua_close(L);
}

void LuaScriptManager::setDirectory(string directory) {
	this->directory = directory;
}

void LuaScriptManager::loadScript(string file, string type) {
	string full_path = directory + "/" + file + ".lua";
	cout << full_path << endl;
	int error = luaL_dofile(L, full_path.c_str());
	if (error != 0) {
        game_manager->logProblem(lua_tostring(L, -1), file, 0);
        lua_pop(L, 1);
    }

	if (type == "update") 
		game_manager->getRenderManager()->updateEveryFrame(file);
}

void LuaScriptManager::inputCallback(string& function, vector<string>* inputs) {
	executeScript(function, inputs, 0);
}

void LuaScriptManager::updateCallback(string& file) {
	string full_function = file + "_update";
	executeScript(full_function, NULL, 0);
}

void LuaScriptManager::eventCallback(string& file, vector<string>* inputs) {
	string full_function = file + "_onEvent";
	executeScript(full_function, inputs, 0);
}

vector<string>* LuaScriptManager::executeScript(string& function, vector<string>* inputs, int num_outputs) {
	int num_inputs = 0;
	vector<string>* outputs = new vector<string>(num_outputs);

	lua_getglobal(L, function.c_str());

	if (inputs != NULL) {
		num_inputs = inputs->size();
		for (int i = 0; i < num_inputs; i++) {
			const char* input = inputs->at(i).c_str();
			lua_pushstring(L, input);
		}
	}

	int error = lua_pcall(L, num_inputs, num_outputs, 0);
	if (error != 0) {
        game_manager->logProblem(lua_tostring(L, -1), function, 0);
        lua_pop(L, 1);
    }

	for (int i = num_outputs - 1; i >= 0; i--) {
		const char* output = lua_tostring(L, -1);
		outputs->push_back(output);
		lua_pop(L, 1);
	}

	return outputs;
}

// vector<string>* LuaScriptManager::executeScript(string& file, string& function, vector<string>* inputs, int num_outputs) {
// 	int num_inputs = inputs->size();
// 	vector<string>* outputs = new vector<string>(num_outputs);

// 	luaL_dofile(L, file.c_str());
// 	lua_getglobal(L, function.c_str());

// 	for (int i = 0; i < num_inputs; i++) {
// 		const char* input = inputs->at(i).c_str();
// 		lua_pushstring(L, input);
// 	}

// 	int error = lua_pcall(L, num_inputs, num_outputs, 0);
// 	if (error != 0) {
//         game_manager->logProblem(lua_tostring(L, -1), function, 0);
//         lua_pop(L, 1);
//     }

// 	for (int i = num_outputs - 1; i >= 0; i--) {
// 		const char* output = lua_tostring(L, -1);
// 		outputs->push_back(output);
// 		lua_pop(L, 1);
// 	}

// 	return outputs;
// }
