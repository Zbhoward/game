#ifndef PHYSICS_MANAGER_H
#define PHYSICS_MANAGER_H

#include <string>

class RenderManager;

class PhysicsManager {
	protected:
		RenderManager* render_manager;

	public:
		PhysicsManager(RenderManager* render_manager) { this->render_manager = render_manager; }
		virtual ~PhysicsManager() { render_manager = NULL; }

		virtual void stepPhysicsSimulation(float time_step) = 0;

		virtual void setDebugMode(int debug) = 0;
		virtual void setGravity(float* gravity) = 0;
		virtual void getGravity(float* gravity) = 0;

		virtual void createCollisionShape(std::string& compound_shape_id, std::string& collision_shape, float* scale, 
			float* translation, float* rotation, float mass) = 0;
		virtual void createRigidBodies() = 0;

		virtual void applyImpulse(const char* rigid_body_name, float ns, float ew, float other) = 0;
		virtual void applyTorqueImpulse(const char* rigid_body_name, float pitch, float yaw, float roll) = 0;
};

#endif