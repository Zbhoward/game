#include <string>

#include "AudioResource.h"

using namespace std;

AudioResource::AudioResource(string& name, string& file_path, string& type) {
	this->name = name;
	this->file_path = file_path;
	if (type == "stream")
		this->type = STREAM;
	else
		this->type = SAMPLE;
}

AudioResource::~AudioResource() {
}

string& AudioResource::getName() {
	return name;
}

string& AudioResource::getFilePath() {
	return file_path;
}

AudioResourceInfo* AudioResource::getInfo() {
	return info;
}

AudioType AudioResource::getType() {
	return type;
}	

void AudioResource::setInfo(AudioResourceInfo* info) {
	this->info = info;
}