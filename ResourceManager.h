#if !defined RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <string>

#include "Level.h"

using namespace std;

class GameManager;
class Level;

class ResourceManager {
	private:
		GameManager* gm;	

		string loaded_level;
		std::map<string, Level*>* levels;

	public:
		enum ResourceType { PATH, MESH };

		ResourceManager(const char* resource_file, GameManager* gm);

		void loadLevel(const char* level_name);
		void unloadLevel(const char* level_name);
};


#endif