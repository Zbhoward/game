#ifndef AUDIO_PLAYER_H
#define AUDIO_PLAYER_H

class AudioResource;

class AudioPlayer {
	private:
		AudioResource* audio;
		int num_repeats;
		int repeat_count;

	public:
		AudioPlayer(AudioResource* audio, int num_plays);
		~AudioPlayer();

		int getNumRepeats();
		int getRepeatCount();
		AudioResource* getAudioResource();
		void incRepeatCount();
};

#endif