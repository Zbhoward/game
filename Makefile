AutomatedMakefile = am
CC = g++

INC_DIRS=-I./ -I$(OGRE_PATH)/OgreMain/include -I$(BOOST_PATH) -I$(TINYXML_PATH)/include -I$(OIS_PATH)/include -I$(BASS_PATH)/include -I$(CEGUI_PATH)/cegui/include -I$(CEGUI_PATH)/build/cegui/include -I$(LUA_PATH)/src -I$(LUA_PATH)/LuaBridge -I$(BULLET_PATH)/src -I$(CSC2110)
LIB_DIRS=-L./ -L$(OGRE_PATH)/build/lib -L$(BOOST_PATH)/stage/lib -L$(TINYXML_PATH)/lib -L$(OIS_PATH)/lib -L$(BASS_PATH)/lib -L$(CEGUI_PATH)/build/lib -L$(LUA_PATH)/lib -L$(BULLET_PATH)/lib
LIBS=-lboost_system-mgw51-mt-1_63 -lOgreMain -ltinyxml -lois -lbass -lCEGUIBase-0 -lCEGUIOgreRenderer-0	-llua -lBulletDynamics -lBulletCollision -lLinearMath

COMPILE = $(CC) $(INC_DIRS) -c
LINK = $(CC) $(LIB_DIRS) -o 

FILES = GameDriver.o GameManager.o OgreRenderManager.o OISInputManager.o ResourceManager.o BassAudioManager.o CEGUIManager.o LuaScriptManager.o BulletPhysicsManager.o Utils.o AnimationRenderListener.o RenderListener.o InputRenderListener.o LogManager.o LevelResources.o Level.o ParseXML.o AudioResource.o AudioPlayer.o BulletDebugDrawer.o BulletSceneNodeMotionState.o CompareCompoundShapes.o CompareRigidBodies.o CompoundShape.o RigidBody.o

all: Ogre

Ogre:	$(FILES)
		$(LINK) Game.exe $(FILES) $(LIBS)
	
GameDriver.o:					GameDriver.cpp
								$(COMPILE) GameDriver.cpp

GameManager.o:					GameManager.h GameManager.cpp
								$(COMPILE) GameManager.cpp
					
ResourceManager.o:				ResourceManager.h ResourceManager.cpp
								$(COMPILE) ResourceManager.cpp

OgreRenderManager.o:			OgreRenderManager.h OgreRenderManager.cpp
								$(COMPILE) OgreRenderManager.cpp

OISInputManager.o:				OISInputManager.h OISInputManager.cpp
								$(COMPILE) OISInputManager.cpp

BassAudioManager.o:				BassAudioManager.h BassAudioManager.cpp
								$(COMPILE) BassAudioManager.cpp

CEGUIManager.o:					CEGUIManager.h CEGUIManager.cpp
								$(COMPILE) CEGUIManager.cpp

LuaScriptManager.o:				LuaScriptManager.h LuaScriptManager.cpp
								$(COMPILE) LuaScriptManager.cpp

BulletPhysicsManager.o:			BulletPhysicsManager.h BulletPhysicsManager.cpp
								$(COMPILE) BulletPhysicsManager.cpp
					
LogManager.o:					LogManager.h LogManager.cpp
								$(COMPILE) LogManager.cpp
					
Utils.o:						Utils.h Utils.cpp
								$(COMPILE) Utils.cpp

RenderListener.o:				RenderListener.h RenderListener.cpp
								$(COMPILE) RenderListener.cpp

AnimationRenderListener.o:		AnimationRenderListener.h AnimationRenderListener.cpp
								$(COMPILE) AnimationRenderListener.cpp

Level.o:						Level.h Level.cpp
								$(COMPILE) Level.cpp

ParseXML.o:						ParseXML.h ParseXML.cpp
								$(COMPILE) ParseXML.cpp

LevelResources.o:				LevelResources.h LevelResources.cpp
								$(COMPILE) LevelResources.cpp

InputRenderListener.o:			InputRenderListener.h InputRenderListener.cpp
								$(COMPILE) InputRenderListener.cpp

AudioResource.o:				AudioResource.h AudioResource.cpp
								$(COMPILE) AudioResource.cpp

AudioPlayer.o:					AudioPlayer.h AudioPlayer.cpp
								$(COMPILE) AudioPlayer.cpp

BulletDebugDrawer.o:			BulletDebugDrawer.h BulletDebugDrawer.cpp
								$(COMPILE) BulletDebugDrawer.cpp

BulletSceneNodeMotionState.o:	BulletSceneNodeMotionState.h BulletSceneNodeMotionState.cpp
								$(COMPILE) BulletSceneNodeMotionState.cpp

CompareCompoundShapes.o:		CompareCompoundShapes.h CompareCompoundShapes.cpp
								$(COMPILE) CompareCompoundShapes.cpp

CompareRigidBodies.o:			CompareRigidBodies.h CompareRigidBodies.cpp
								$(COMPILE) CompareRigidBodies.cpp

CompoundShape.o:				CompoundShape.h CompoundShape.cpp
								$(COMPILE) CompoundShape.cpp

RigidBody.o:					RigidBody.h RigidBody.cpp
								$(COMPILE) RigidBody.cpp


.PHONY: clean
clean:
	del *.o *.log *.exe
