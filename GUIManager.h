#ifndef GUI_MANAGER_H
#define GUI_MANAGER_H

#include "GameHeader.h"
#include "InputManager.h"
#include <string>

class GameManager;

class GUIManager {
	protected:
		GameManager* game_manager;

	public:
		GUIManager(GameManager* game_manager) { this->game_manager = game_manager; }
		virtual ~GUIManager() { game_manager = NULL; }

		virtual void loadGUI(std::string& level, std::string& scheme, std::string& font, std::string& cursor, std::string& tooltip, std::string& layout) = 0;
		virtual void unloadGUI() = 0;

		virtual bool isElementActive(std::string element) = 0;

		virtual void keyPressed(Key key) = 0;
		virtual void mouseMoved(uint32 mouse_x, uint32 mouse_y, int mouse_rel_x, int mouse_rel_y) = 0;
        virtual void mousePressed(uint32 x_click, uint32 y_click, uint32 button) = 0;

        virtual void processEvent(std::string& type, std::string& name, std::string& script) = 0;
};

#endif