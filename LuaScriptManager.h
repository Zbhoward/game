#ifndef LUA_SCRIPT_MANAGER
#define LUA_SCRIPT_MANAGER

extern "C"
{
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}

#include <LuaBridge.h>
#include "ScriptManager.h"

#include <string>
#include <vector>

class GameManager;

class LuaScriptManager : public ScriptManager {
	private:
		lua_State* L;
		std::string directory;

	public:
		LuaScriptManager(GameManager* game_manager);
		virtual ~LuaScriptManager();

		void setDirectory(std::string directory);
		void loadScript(std::string path, std::string type);

		void inputCallback(std::string& function, std::vector<std::string>* inputs);
		void updateCallback(std::string& file);
        void eventCallback(std::string& file, std::vector<std::string>* inputs);
        std::vector<std::string>* executeScript(std::string& script_function, 
				std::vector<std::string>* inputs, int num_outputs);
		// std::vector<std::string>* executeScript(std::string& script_file, std::string& script_function, 
		// 		std::vector<std::string>* inputs, int num_outputs);
};

#endif