#ifndef INPUT_RENDER_LISTENER_H
#define INPUT_RENDER_LISTENER_H

#include "RenderListener.h"

class InputRenderListener : public RenderListener {
	private:

	public:
		InputRenderListener(RenderManager* render_manager);
		virtual ~InputRenderListener();

		virtual bool frameStarted(const Ogre::FrameEvent& event);
};

#endif