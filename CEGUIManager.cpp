#include "CEGUIManager.h"
#include "RenderManager.h"
#include "AudioManager.h"
#include "InputManager.h"
#include "ScriptManager.h"
#include "Utils.h"
#include <iostream>
#include <string>

using namespace std;

CEGUIManager::CEGUIManager(GameManager* game_manager) : GUIManager(game_manager) {
    root_window = NULL;
    gui_context = NULL;

    cegui_ogre_renderer = getRenderer();

    events = new map<string, string>();
}

CEGUIManager::~CEGUIManager() {
}

void CEGUIManager::createGUIContext(string& scheme, string& font, string& cursor, string& tooltip, string& layout) {
	CEGUI::SchemeManager::getSingleton().createFromFile(scheme + ".scheme");
	CEGUI::FontManager::getSingleton().createFromFile(font + ".font");

	CEGUI::System& system = CEGUI::System::getSingleton();
	gui_context = &system.createGUIContext(cegui_ogre_renderer->getDefaultRenderTarget());
	gui_context->setDefaultFont(font);
	gui_context->getMouseCursor().setDefaultImage(cursor);
	gui_context->setDefaultTooltipType(tooltip);

	CEGUI::WindowManager* win_mgr = &CEGUI::WindowManager::getSingleton();
	root_window = win_mgr->loadLayoutFromFile(layout + ".layout");
	gui_context->setRootWindow(root_window);

    CEGUI::GlobalEventSet::getSingleton().subscribeEvent(CEGUI::Window::EventNamespace + "/" + CEGUI::Window::EventMouseButtonDown,
        CEGUI::Event::Subscriber(&CEGUIManager::mousePressedEvent, this));
}

void CEGUIManager::destroyGUIContext() {
	CEGUI::System& system = CEGUI::System::getSingleton();
	if (gui_context != NULL) {
        system.destroyGUIContext(*gui_context);
        gui_context = NULL;
	}

    CEGUI::SchemeManager::getSingleton().destroyAll();
    CEGUI::FontManager::getSingleton().destroyAll();
}

void CEGUIManager::loadGUI(string& level, string& scheme, string& font, string& cursor, string& tooltip, string& layout) {
    if (gui_context != NULL) return;

	CEGUI::ImageManager::setImagesetDefaultResourceGroup(level);
    CEGUI::Scheme::setDefaultResourceGroup(level);
    CEGUI::Font::setDefaultResourceGroup(level);
    CEGUI::WidgetLookManager::setDefaultResourceGroup(level);
    CEGUI::WindowManager::setDefaultResourceGroup(level);
    CEGUI::ScriptModule::setDefaultResourceGroup(level);

    createGUIContext(scheme, font, cursor, tooltip, layout);
}

void CEGUIManager::unloadGUI() {
    if (gui_context != NULL) {
        destroyGUIContext();
        gui_context = NULL;
    }
}

bool CEGUIManager::isElementActive(string element) {
    CEGUI::Window* gui_element = root_window->getChild(element);
    return gui_element->isActive();
}

void CEGUIManager::buttonEvent(const CEGUI::EventArgs& e) {
    CEGUI::Editbox* editbox = static_cast<CEGUI::Editbox*>(root_window->getChild("FrameWindow/Editbox"));
    CEGUI::Window* error = static_cast<CEGUI::Window*>(root_window->getChild("FrameWindow/Error"));
    CEGUI::String address = editbox->getText();

    if (address.size() <= 6) {
        error->setText("Must be 7 characters!");
        return;
    }

    for (int i = 0; i < address.size(); i++) {
        char curr = address[i];
        if ((curr < 62 || curr > 90) && (curr < 48 || curr > 57)) {
            error->setText("Contains an invalid character!");
            return;
        }
    }

    error->setText("");
    string address_str = address.c_str();

    string full_path = "FrameWindow/Button-ButtonEvent";
    string script = events->at(full_path);
    vector<string>* input = new vector<string>();
    input->push_back(address_str);
    cout << script << endl;
    game_manager->getScriptManager()->eventCallback(script, input);
}

void CEGUIManager::scrollEvent(const CEGUI::EventArgs& e) {
    const CEGUI::WindowEventArgs& we = static_cast<const CEGUI::WindowEventArgs&>(e);
	CEGUI::Scrollbar* scroll_bar = static_cast<CEGUI::Scrollbar*>(we.window);
	float position = scroll_bar->getUnitIntervalScrollPosition();

    string full_path = getFullName(we.window) + "-ScrollEvent";
    string script = events->at(full_path);

    vector<string>* input = new vector<string>();
    string position_str = Utils::f_to_s(position);
    input->push_back(position_str);
    cout << script << endl;
    game_manager->getScriptManager()->eventCallback(script, input);
}

void CEGUIManager::mousePressedEvent(const CEGUI::EventArgs& e) {
    const CEGUI::MouseEventArgs& mouseeventargs = static_cast<const CEGUI::MouseEventArgs&>(e);
    CEGUI::Window* editbox = root_window->getChild("FrameWindow/Editbox");

    if (mouseeventargs.button == CEGUI::LeftButton && editbox->isActive() && mouseeventargs.window != editbox) {
        editbox->deactivate();
        return;
    } /*else if (!editbox->isActive() && mouseeventargs.window == editbox) {
        cout << "enabling GUI" << endl;
        editbox->activate();
    }*/
}

void CEGUIManager::keyPressed(Key key) {
    if (key == K_ESCAPE) {
        game_manager->getRenderManager()->stopRendering();
        return;
    }/* else if (key == K_SPACE) {
        game_manager->getRenderManager()->startAnimation("Inner Lock");
        game_manager->getRenderManager()->startAnimation("Outer Lock");
        game_manager->getAudioManager()->playAudio("Chevron lock", 1);
        return;
    }*/
    /*else if (key == K_VOLUMEUP)
        adjustVolume(1)
    else if (key == K_VOLUMEDOWN)
        adjustVolume(-1);*/

    CEGUI::Window* editbox = root_window->getChild("FrameWindow/Editbox");
    if (editbox->isActive()) {
        char key_char = InputManager::keyToChar(key);
        if (key_char != 0)
            gui_context->injectChar(key_char);
        else {
            gui_context->injectKeyDown(specialKeyMap(key));
            gui_context->injectKeyUp(specialKeyMap(key));
        }
    } else {
        string function = "keyPressed";
        vector<string>* inputs = new vector<string>();
        string input(1, InputManager::keyToChar(key));
        inputs->push_back(input);
        game_manager->getScriptManager()->inputCallback(function, inputs);
    }
}

void CEGUIManager::mousePressed(uint32 x_click, uint32 y_click, uint32 button) {
	CEGUI::MouseButton mouse_enum = CEGUI::NoButton;
	if (button == 1)
		mouse_enum = CEGUI::LeftButton;
	else if (button == 2)
		mouse_enum = CEGUI::RightButton;

	gui_context->injectMouseButtonDown(mouse_enum);
	gui_context->injectMouseButtonUp(mouse_enum);
}

void CEGUIManager::mouseMoved(uint32 x_click, uint32 y_click, int x_rel, int y_rel) {
	gui_context->injectMouseMove(x_rel, y_rel);
}

string CEGUIManager::getFullName(CEGUI::Window* window) {
    CEGUI::Window* curr_element = window;
    CEGUI::String full_name = "";
    CEGUI::String curr_name = curr_element->getName();

    while (curr_name != root_window->getName()) {
        if (full_name == "")
            full_name = curr_name;
        else
            full_name = curr_name + "/" + full_name;
        curr_element = curr_element->getParent();
        curr_name = curr_element->getName();
    }
    return string(full_name.c_str());
}

void CEGUIManager::processEvent(std::string& type, std::string& name, std::string& script) {
    if (type == "ButtonEvent") {
        CEGUI::PushButton* push_button = static_cast<CEGUI::PushButton*>(root_window->getChild(name));
        push_button->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&CEGUIManager::buttonEvent, this));
    } else if (type == "ScrollEvent") {
        CEGUI::Scrollbar* scroll_bar = static_cast<CEGUI::Scrollbar*>(root_window->getChild(name));
        scroll_bar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged, CEGUI::Event::Subscriber(&CEGUIManager::scrollEvent, this));
    }

    string key = name + "-" + type;
    events->insert(std::pair<string, string>(key, script));
}

CEGUI::OgreRenderer* CEGUIManager::getRenderer() {
	static CEGUI::OgreRenderer& cegui_ogre_renderer = CEGUI::OgreRenderer::bootstrapSystem();
	return &cegui_ogre_renderer;
}

CEGUI::Key::Scan CEGUIManager::specialKeyMap(Key key) {
    CEGUI::Key::Scan _key = CEGUI::Key::Unknown;
    switch (key) {
        case K_BACK:     _key = CEGUI::Key::Backspace; break;
        case K_TAB:      _key = CEGUI::Key::Tab; break;
        case K_RETURN:   _key = CEGUI::Key::Return; break;
        case K_LCONTROL: _key = CEGUI::Key::LeftControl; break;
        case K_LSHIFT:   _key = CEGUI::Key::LeftShift; break;
        case K_RSHIFT:   _key = CEGUI::Key::RightShift; break;
        case K_LMENU:    _key = CEGUI::Key::LeftAlt; break;
        case K_CAPITAL:  _key = CEGUI::Key::Capital; break;
        case K_RCONTROL: _key = CEGUI::Key::RightControl; break;
        case K_RMENU:    _key = CEGUI::Key::RightAlt; break;
        case K_UP:       _key = CEGUI::Key::ArrowUp; break;
        case K_LEFT:     _key = CEGUI::Key::ArrowLeft; break;
        case K_RIGHT:    _key = CEGUI::Key::ArrowRight; break;
        case K_DOWN:     _key = CEGUI::Key::ArrowDown; break;
        case K_DELETE:   _key = CEGUI::Key::Delete; break;
    }
}