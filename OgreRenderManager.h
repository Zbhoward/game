#if !defined OGRE_RENDER_MANAGER_H
#define OGRE_RENDER_MANAGER_H

#include "Ogre.h"
#include "tinyxml.h"
#include "RenderListener.h"
#include "GameHeader.h"
#include "RenderManager.h"

#include <string>
#include <vector>

using namespace std;

class GameManager;
class GUIManager;
class PhysicsManager;
struct SceneNodeMotion;
struct SceneNodeManual;

class OgreRenderManager : public RenderManager
{
	private:
		// GameManager* game_manager;
		PhysicsManager* physics_manager;

		Ogre::Root* root;
		Ogre::RenderWindow* window;
		Ogre::SceneManager* scene_manager;

		Ogre::Camera* camera;
		Ogre::Viewport* viewport;

        vector<RenderListener*>* render_listeners;
        vector<string>* scripts_to_update;

        bool animation_playing;
		vector<Ogre::AnimationState*>* animation_states;

		void init();
		size_t window_handle;
		Ogre::Real time_since_last_frame;

		Ogre::TransformKeyFrame* getKeyFrame(string& animation_name, int frame_index);

	public:
		OgreRenderManager(GameManager* game_manager);
		virtual ~OgreRenderManager();

		size_t getRenderWindowHandle();
		int getRenderWindowWidth();
		int getRenderWindowHeight();

		Ogre::RenderWindow* getRenderWindow();
		Ogre::SceneManager* getSceneManager();
		
		void startRendering();
		void stopRendering();

		PhysicsManager* getPhysicsManager();

        //Methods for scenes
		void createCamera(float* position, float* look_at, float* clip_distances);
		void createLight(string& name, float* ambient_color, float* diffuse_color, float* direction);
		void createEntity(string& name, string& mesh, string& material, string& scene_node);
		void createSceneNode(string& child, string& parent);
		void createAnimation(string& name, float length, string& scene_node);
		void createAnimationKeyFrame(string& animation_name, float time);
		void createAnimationState(string& animation_name);

		void processTranslate(string& scene_node, float* values);
		void processRotate(string& scene_node, float* values);
		void processScale(string& scene_node, float* values);

		void processAnimationTranslate(string& animation_name, int frame_index, float* values);
		void processAnimationRotate(string& animation_name, int frame_index, float* values);
		void processAnimationScale(string& animation_name, int frame_index, float* values);

		void updateAnimations(float time_step);
		void updateEveryFrame(string script);

		void loadResources(const char* level_name, vector<string*>* paths, vector<string*>* meshes);
        void unloadResources(const char* level_name);

        //Methods for input
        void checkForInput(float time_step);

        void rotateMesh(string node, float value, int x, int y, int z);
        void startAnimation(string animation_name);
        void stopAnimation(string animation_name);

        void loadGUI(string& level, string& scheme, string& font, string& cursor, string& tooltip, string& layout);

        void drawLine(float* from, float* to, float* color, SceneNodeManual* snm);

		SceneNodeMotion* createSceneNodeMotion(std::string& scene_node_id);
		void destroySceneNodeMotion(SceneNodeMotion* snm);
		SceneNodeManual* createManualObject();
		void clearManualObject(SceneNodeManual* snm);
		
		void setPosition(SceneNodeMotion* scene_node_motion, double x, double y, double z);
		float* getPosition(SceneNodeMotion* scene_node_motion);
		void setOrientation(SceneNodeMotion* scene_node_motion, double w, double x, double y, double z);
		float* getOrientation(SceneNodeMotion* scene_node_motion);
};

#endif
