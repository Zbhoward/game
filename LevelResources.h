#if !defined LEVEL_RESOURCES_H
#define LEVEL_RESOURCES_H

#include <vector>
#include <string>

#include "AudioResource.h"

using namespace std;

class LevelResources {

	private:
		vector<string*>* paths;
		vector<string*>* meshes;
		vector<AudioResource*>* audio;

	public:
		LevelResources(vector<string*>* paths, vector<string*>* meshes, vector<AudioResource*>* audio);
		~LevelResources();

		vector<string*>* getPaths();
		vector<string*>* getMeshes();
		vector<AudioResource*>* getAudio();
};

#endif