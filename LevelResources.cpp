#include "LevelResources.h"
#include "AudioResource.h"

using namespace std;

LevelResources::LevelResources(vector<string*>* paths, vector<string*>* meshes, vector<AudioResource*>* audio) {
	this->paths = paths;
	this->meshes = meshes;
	this->audio = audio;
}

LevelResources::~LevelResources() {
	delete paths;
	delete meshes;
	delete audio;
}

vector<string*>* LevelResources::getPaths() {
	return paths;
}

vector<string*>* LevelResources::getMeshes() {
	return meshes;
}

vector<AudioResource*>* LevelResources::getAudio() {
	return audio;
}