#include <sstream>
#include <iostream>
#include <math.h>

#include "GameHeader.h"
#include "OISInputManager.h"
#include "GameManager.h"
#include "GUIManager.h"
#include "ScriptManager.h"
#include "Utils.h"

using namespace std;

OISInputManager::OISInputManager(GameManager* gm) : InputManager(gm) {
	ois_input_manager = NULL;
	keyboard = NULL;
	mouse = NULL;
	joystick = NULL;

    left_axis_moved = false;

	init();

	window_width = game_manager->getRenderWindowWidth();
	window_height = game_manager->getRenderWindowHeight();
}
 
OISInputManager::~OISInputManager() {
	if (ois_input_manager)
		ois_input_manager->destroyInputSystem(ois_input_manager);

	ois_input_manager = NULL;
	keyboard = NULL;
	mouse = NULL;
	joystick = NULL;
}

void OISInputManager::init() {
	try {
		OIS::ParamList params;
		ostringstream windowHndStr;
		size_t window_handle = game_manager->getRenderWindowHandle();

		size_t handle = window_handle;
		windowHndStr << handle;
		params.insert(make_pair(string("WINDOW"), windowHndStr.str()));

		ois_input_manager = OIS::InputManager::createInputSystem(params);
		if (ois_input_manager->getNumberOfDevices(OIS::OISKeyboard) > 0) {
			keyboard = static_cast<OIS::Keyboard*>(ois_input_manager->createInputObject(OIS::OISKeyboard, true));
			keyboard->setEventCallback(this);
		}

		if (ois_input_manager->getNumberOfDevices(OIS::OISMouse) > 0) {
			mouse = static_cast<OIS::Mouse*>(ois_input_manager->createInputObject(OIS::OISMouse, true));
			mouse->setEventCallback(this);

            const OIS::MouseState& mouse_state = mouse->getMouseState();
            mouse_state.width = window_width;
            mouse_state.height = window_height;
		}

		if (ois_input_manager->getNumberOfDevices(OIS::OISJoyStick) > 0) {
			joystick = static_cast<OIS::JoyStick*>(ois_input_manager->createInputObject(OIS::OISJoyStick, true));
			joystick->setEventCallback(this);
		}
	} catch(exception& e) {
		ASSERT_CRITICAL(false, e.what());
	} catch(...) {
		ASSERT_CRITICAL(false, "Input Manager Initialization Error");
	}
}

void OISInputManager::free() {
	if (ois_input_manager)
		ois_input_manager->destroyInputSystem(ois_input_manager);
}

void OISInputManager::checkForInput() {
	if (keyboard) keyboard->capture();
	if (mouse) mouse->capture();
	if (joystick) joystick->capture();

    // if (keyboard->isKeyDown(OIS::KC_LEFT))
    //     game_manager->getGUIManager()->keyPressed(K_LEFT); 
    // if (keyboard->isKeyDown(OIS::KC_RIGHT))
    //     game_manager->getGUIManager()->keyPressed(K_RIGHT);
}

bool OISInputManager::keyPressed(const OIS::KeyEvent& e) {
    game_manager->getGUIManager()->keyPressed(keyMap(e));
	return true;
}

bool OISInputManager::keyReleased(const OIS::KeyEvent& e) {
    // game_manager->keyReleased(keyMap(e));
    return true;
}

bool OISInputManager::mouseMoved(const OIS::MouseEvent& e) {
    e.state.width = window_width;
    e.state.height = window_height;

    uint32 x_click = e.state.X.abs;
    uint32 y_click = e.state.Y.abs;
    int x_rel = (int) e.state.X.rel;
    int y_rel = (int) e.state.Y.rel;
    game_manager->getGUIManager()->mouseMoved(x_click, y_click, x_rel, y_rel);
    
    return true;
}

bool OISInputManager::mousePressed(const OIS::MouseEvent& e, OIS::MouseButtonID button) {
    uint32 x_click = e.state.X.abs;
    uint32 y_click = e.state.Y.abs;
    game_manager->getGUIManager()->mousePressed(x_click, y_click, mouseMap(button));
    return true;
}

bool OISInputManager::mouseReleased(const OIS::MouseEvent& e, OIS::MouseButtonID button) {
    // uint32 x_click = e.state.X.abs;
    // uint32 y_click = e.state.Y.abs;
    // game_manager->getGUIManager()->mouseReleased(x_click, y_click, mouseMap(button));
    return true;
}

bool OISInputManager::buttonPressed(const OIS::JoyStickEvent& e, int button) {
    string function = "buttonPressed";
    vector<string>* input = new vector<string>();
    input->push_back(joystickButtonMap(button));
    game_manager->getScriptManager()->inputCallback(function, input);
    return true;
}

bool OISInputManager::buttonReleased(const OIS::JoyStickEvent& e, int button) {
    // game_manager->buttonReleased(joystickButtonMap(button));
    return true;
}

bool OISInputManager::sliderMoved(const OIS::JoyStickEvent& e, int index) {
    return true;
}

bool OISInputManager::povMoved(const OIS::JoyStickEvent& e, int index) {
    return true;
}

bool OISInputManager::vector3Moved(const OIS::JoyStickEvent& e, int index) {
    return true;
}

bool OISInputManager::axisMoved(const OIS::JoyStickEvent& e, int axis) {
    const int AXIS_DEADZONE = 10000;
    const int TRIGGER_DEADZONE = 30;
    
    int amount[6] = {e.state.mAxes[0].abs, e.state.mAxes[1].abs,
                     e.state.mAxes[2].abs, e.state.mAxes[3].abs,
                     e.state.mAxes[4].abs, e.state.mAxes[5].abs};

    bool needsUpdate = false;
    float north_south = 0;
    float east_west = 0;
    if (fabs(amount[0]) > AXIS_DEADZONE) {
        north_south = amount[0] / 32767.0;
        needsUpdate = true;
    }

    if (fabs(amount[1]) > AXIS_DEADZONE) {
        east_west = amount[1] / 32767.0;
        needsUpdate = true;
        // left_axis_moved = true;
    } /*else {
        if (left_axis_moved) {
            game_manager->getScriptManager()->
            left_axis_moved = false;
        }
    }*/

    if (needsUpdate) {
        string function = "leftAxisMoved";
        vector<string>* inputs = new vector<string>();
        inputs->push_back(Utils::f_to_s(north_south));
        inputs->push_back(Utils::f_to_s(east_west));
        game_manager->getScriptManager()->inputCallback(function, inputs);
    }

    needsUpdate = false;
    north_south = 0;
    east_west = 0;
    if (fabs(amount[2]) > AXIS_DEADZONE) {
        north_south = amount[2] / 32767.0;
        needsUpdate = true;
    }

    if (fabs(amount[3]) > AXIS_DEADZONE) {
        east_west = amount[3] / 32767.0;
        needsUpdate = true;
    }

    if (needsUpdate) {
        string function = "rightAxisMoved";
        vector<string>* inputs = new vector<string>();
        inputs->push_back(Utils::f_to_s(north_south));
        inputs->push_back(Utils::f_to_s(east_west));
        game_manager->getScriptManager()->inputCallback(function, inputs);
    }

    return true;
}

string OISInputManager::joystickButtonMap(int button) {
    string _key = "INVALID_KEY";
    switch(button) {
        case 0: _key = "A"; break;
        case 1: _key = "B"; break;
        case 2: _key = "X"; break;
        case 3: _key = "Y"; break;
        case 4: _key = "LB"; break;
        case 5: _key = "RB"; break;
        case 6: _key = "SELECT"; break;
        case 7: _key = "START"; break;
        case 8: _key = "L_STICK"; break;
        case 9: _key = "R_STICK"; break;
    }

    return _key;
}

Key OISInputManager::keyMap(const OIS::KeyEvent& e) {
	Key _key = K_UNASSIGNED;
	OIS::KeyCode key_code = e.key;

    switch(key_code) {
        case OIS::KC_UNASSIGNED:   _key = K_UNASSIGNED; break;
        case OIS::KC_ESCAPE:       _key = K_ESCAPE; break;
        case OIS::KC_1:            _key = K_1; break;
        case OIS::KC_2:            _key = K_2; break;
        case OIS::KC_3:            _key = K_3; break;
        case OIS::KC_4:            _key = K_4; break;
        case OIS::KC_5:            _key = K_5; break;
        case OIS::KC_6:            _key = K_6; break;
        case OIS::KC_7:            _key = K_7; break;
        case OIS::KC_8:            _key = K_8; break;
        case OIS::KC_9:            _key = K_9; break;
        case OIS::KC_0:            _key = K_0; break;
        case OIS::KC_MINUS:        _key = K_MINUS; break;    // - on main keyboard
        case OIS::KC_EQUALS:       _key = K_EQUALS; break;
        case OIS::KC_BACK:         _key = K_BACK; break;    // backspace
        case OIS::KC_TAB:          _key = K_TAB; break;
        case OIS::KC_Q:            _key = K_Q; break;
        case OIS::KC_W:            _key = K_W; break;
        case OIS::KC_E:            _key = K_E; break;
        case OIS::KC_R:            _key = K_R; break;
        case OIS::KC_T:            _key = K_T; break;
        case OIS::KC_Y:            _key = K_Y; break;
        case OIS::KC_U:            _key = K_U; break;
        case OIS::KC_I:            _key = K_I; break;
        case OIS::KC_O:            _key = K_O; break;
        case OIS::KC_P:            _key = K_P; break;
        case OIS::KC_LBRACKET:     _key = K_LBRACKET; break;
        case OIS::KC_RBRACKET:     _key = K_RBRACKET; break;
        case OIS::KC_RETURN:       _key = K_RETURN; break;    // Enter on main keyboard
        case OIS::KC_LCONTROL:     _key = K_LCONTROL; break;
        case OIS::KC_A:            _key = K_A; break;
        case OIS::KC_S:            _key = K_S; break;
        case OIS::KC_F:            _key = K_F; break;
        case OIS::KC_D:            _key = K_D; break;
        case OIS::KC_G:            _key = K_G; break;
        case OIS::KC_H:            _key = K_H; break;
        case OIS::KC_J:            _key = K_J; break;
        case OIS::KC_K:            _key = K_K; break;
        case OIS::KC_L:            _key = K_L; break;
        case OIS::KC_SEMICOLON:    _key = K_SEMICOLON; break;
        case OIS::KC_APOSTROPHE:   _key = K_APOSTROPHE; break;
        case OIS::KC_GRAVE:        _key = K_GRAVE; break;    // accent
        case OIS::KC_LSHIFT:       _key = K_LSHIFT; break;
        case OIS::KC_BACKSLASH:    _key = K_BACKSLASH; break;
        case OIS::KC_Z:            _key = K_Z; break;
        case OIS::KC_X:            _key = K_X; break;
        case OIS::KC_C:            _key = K_C; break;
        case OIS::KC_V:            _key = K_V; break;
        case OIS::KC_B:            _key = K_B; break;
        case OIS::KC_N:            _key = K_N; break;
        case OIS::KC_M:            _key = K_M; break;
        case OIS::KC_COMMA:        _key = K_COMMA; break;
        case OIS::KC_PERIOD:       _key = K_PERIOD; break;    // . on main keyboard
        case OIS::KC_SLASH:        _key = K_SLASH; break;    // / on main keyboard
        case OIS::KC_RSHIFT:       _key = K_RSHIFT; break;
        case OIS::KC_MULTIPLY:     _key = K_MULTIPLY; break;    // * on numeric keypad
        case OIS::KC_LMENU:        _key = K_LMENU; break;    // left Alt
        case OIS::KC_SPACE:        _key = K_SPACE; break;
        case OIS::KC_CAPITAL:      _key = K_CAPITAL; break;
        case OIS::KC_NUMPAD7:      _key = K_NUMPAD7; break;
        case OIS::KC_NUMPAD8:      _key = K_NUMPAD8; break;
        case OIS::KC_NUMPAD9:      _key = K_NUMPAD9; break;
        case OIS::KC_SUBTRACT:     _key = K_SUBTRACT; break;    // - on numeric keypad
        case OIS::KC_NUMPAD4:      _key = K_NUMPAD4; break;
        case OIS::KC_NUMPAD5:      _key = K_NUMPAD5; break;
        case OIS::KC_NUMPAD6:      _key = K_NUMPAD6; break;
        case OIS::KC_ADD:          _key = K_ADD; break;    // + on numeric keypad
        case OIS::KC_NUMPAD1:      _key = K_NUMPAD1; break;
        case OIS::KC_NUMPAD2:      _key = K_NUMPAD2; break;
        case OIS::KC_NUMPAD3:      _key = K_NUMPAD3; break;
        case OIS::KC_NUMPAD0:      _key = K_NUMPAD0; break;
        case OIS::KC_DECIMAL:      _key = K_DECIMAL; break;    // . on numeric keypad
        case OIS::KC_AT:           _key = K_AT; break;    //                     (NEC PC98)
        case OIS::KC_COLON:        _key = K_COLON; break;    //                     (NEC PC98)
        case OIS::KC_UNDERLINE:    _key = K_UNDERLINE; break;    //                     (NEC PC98)
        case OIS::KC_RCONTROL:     _key = K_RCONTROL; break;
        case OIS::KC_VOLUMEDOWN:   _key = K_VOLUMEDOWN; break;    // Volume -
        case OIS::KC_VOLUMEUP:     _key = K_VOLUMEUP; break;    // Volume +
        case OIS::KC_DIVIDE:       _key = K_DIVIDE; break;    // / on numeric keypad
        case OIS::KC_RMENU:        _key = K_RMENU; break;    // right Alt
        case OIS::KC_UP:           _key = K_UP; break;    // UpArrow on arrow keypad
        case OIS::KC_LEFT:         _key = K_LEFT; break;    // LeftArrow on arrow keypad
        case OIS::KC_RIGHT:        _key = K_RIGHT; break;    // RightArrow on arrow keypad
        case OIS::KC_DOWN:         _key = K_DOWN; break;    // DownArrow on arrow keypad
        case OIS::KC_DELETE:       _key = K_DELETE; break;    // Delete on arrow keypad
    }

    return _key;
}

uint32 OISInputManager::mouseMap(const OIS::MouseButtonID button) {
    return button + 1;
}