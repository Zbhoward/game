#include <iostream>
#include <string>
#include <map>

#include "ParseXML.h"
#include "LevelResources.h"
#include "AudioResource.h"
#include "Utils.h"
#include "GameManager.h"
#include "RenderManager.h"
#include "GUIManager.h"
#include "ScriptManager.h"
#include "PhysicsManager.h"

using namespace std;

ParseXML::ParseXML(GameManager* gm) {
	game_manager = gm;
}

map<string, Level*>* ParseXML::parseResourceXML(const char* file_name) {
	map<string, Level*>* levels = new map<string, Level*>();

	TiXmlDocument doc(file_name);
	if (!doc.LoadFile()) return NULL;
	
	TiXmlElement* levels_element = doc.RootElement();
	TiXmlElement* level_element = levels_element->FirstChildElement("level");
	
	while (level_element != NULL) {
		vector<string*>* paths = new vector<string*>();
		vector<string*>* meshes = new vector<string*>();
		vector<AudioResource*>* audio = new vector<AudioResource*>();

		string* level_name = new string(level_element->FirstChildElement("name")->GetText());
		
		TiXmlElement* paths_element = level_element->FirstChildElement("paths");
		TiXmlElement* path_element = paths_element->FirstChildElement("path");
		
		while (path_element != NULL) {
			string* path = new string(path_element->GetText());
			paths->push_back(path);
			path_element = path_element->NextSiblingElement();
		}
		
		TiXmlElement* meshes_element = level_element->FirstChildElement("meshes");
		TiXmlElement* mesh_element = meshes_element->FirstChildElement("mesh");
		
		while (mesh_element != NULL) {
			string* mesh = new string(mesh_element->GetText());
			meshes->push_back(mesh);
			mesh_element = mesh_element->NextSiblingElement();
		}
		
		TiXmlElement* audios_element = level_element->FirstChildElement("audios");
		if (audios_element != NULL) {
			TiXmlElement* audio_element = audios_element->FirstChildElement("audio");
			
			while (audio_element != NULL) {
				string audio_name = audio_element->FirstChildElement("name")->GetText();
				string audio_file = audio_element->FirstChildElement("file")->GetText();
				string audio_type = audio_element->FirstChildElement("type")->GetText();

				audio->push_back(new AudioResource(audio_name, audio_file, audio_type));
				audio_element = audio_element->NextSiblingElement();
			}
		}
		
		Level* level = new Level(level_name->c_str(), new LevelResources(paths, meshes, audio));
		levels->insert(std::pair<string, Level*>((*level_name), level));

		level_element = levels_element->NextSiblingElement();
	}

	return levels;
}

void ParseXML::parseSceneXML(const char* file_name) {	
	level_name = string(file_name);
	string path = level_name + ".xml";
	TiXmlDocument doc(path.c_str());
	if (!doc.LoadFile()) return;
	
	TiXmlElement* scene_element = doc.RootElement();
	TiXmlElement* camera_element = scene_element->FirstChildElement("camera");
	processCamera(camera_element);
	
	TiXmlElement* lights_element = scene_element->FirstChildElement("lights");
	processLights(lights_element);

	TiXmlElement* scripts_element = scene_element->FirstChildElement("scripts");
	processScripts(scripts_element);
	
	TiXmlElement* gui_element = scene_element->FirstChildElement("gui");
	processGUI(gui_element);

	TiXmlElement* graph_element = scene_element->FirstChildElement("graph");	
	TiXmlElement* children_element = graph_element->FirstChildElement("children");
	
	string root = "root";

	if (children_element != NULL)
		processChild(children_element, root);
	
	cout << "Done parsing " << file_name << endl << endl;
}

void ParseXML::processCamera(TiXmlElement* camera_element) {	
	string position_str = camera_element->FirstChildElement("position")->GetText();
	float* position = new float[3];
	Utils::parseFloats(position_str, position);
	
	string lookAt_str = camera_element->FirstChildElement("lookingAt")->GetText();
	float* look_at = new float[3];
	Utils::parseFloats(lookAt_str, look_at);
	
	string clip_str = camera_element->FirstChildElement("clip")->GetText();
	float* clips = new float[2];
	Utils::parseFloats(clip_str, clips);
	
	game_manager->getRenderManager()->createCamera(position, look_at, clips);

	delete[] position;
	delete[] look_at;
	delete[] clips;
}

void ParseXML::processLights(TiXmlElement* lights_element) {	
	string ambient_colour_str = lights_element->FirstChildElement("ambientLightColour")->GetText();
	float* ambient_color = new float[3];
	Utils::parseFloats(ambient_colour_str, ambient_color);
	
	TiXmlElement* light_element = lights_element->FirstChildElement("light");
	while (light_element != NULL) {
		string name = light_element->FirstChildElement("name")->GetText();
		
		string diffuse_colour_str = light_element->FirstChildElement("diffuseColour")->GetText();
		float* diffuse_color = new float[3];
		Utils::parseFloats(diffuse_colour_str, diffuse_color);
		
		string direction_str = light_element->FirstChildElement("direction")->GetText();
		float* direction = new float[3];
		Utils::parseFloats(direction_str, direction);
		
		game_manager->getRenderManager()->createLight(name, ambient_color, diffuse_color, direction);

		light_element = light_element->NextSiblingElement();

		delete[] diffuse_color;
		delete[] direction;
	}

	delete[] ambient_color;
}

void ParseXML::processScripts(TiXmlElement* scripts_element) {
	if (scripts_element != NULL) {
		string directory = scripts_element->FirstChildElement("dir")->GetText();
		game_manager->getScriptManager()->setDirectory(directory);

		TiXmlElement* script_element = scripts_element->FirstChildElement("script");
		while (script_element != NULL) {
			string path = script_element->FirstChildElement("file")->GetText();
			string type = script_element->FirstChildElement("type")->GetText();
			game_manager->getScriptManager()->loadScript(path, type);

			script_element = script_element->NextSiblingElement();
		}
	}
}

void ParseXML::processGUI(TiXmlElement* gui_element) {
	if (gui_element != NULL) {
		string scheme = gui_element->FirstChildElement("scheme")->GetText();
		string font = gui_element->FirstChildElement("font")->GetText();
		string cursor = gui_element->FirstChildElement("cursor")->GetText();
		string tooltip = gui_element->FirstChildElement("tooltip")->GetText();
		string layout = gui_element->FirstChildElement("layout")->GetText();

		game_manager->getGUIManager()->loadGUI(level_name, scheme, font, cursor, tooltip, layout);

        TiXmlElement* events_element = (TiXmlElement*) gui_element->FirstChild("events");
        TiXmlElement* event_element = (TiXmlElement*) events_element->FirstChild("event");
        while (event_element != NULL) {
            string type = event_element->FirstChildElement("type")->GetText();
            string name = event_element->FirstChildElement("name")->GetText();
            string script = event_element->FirstChildElement("script")->GetText();

            game_manager->getGUIManager()->processEvent(type, name, script);
            event_element = event_element->NextSiblingElement();
        }
	}
}

void ParseXML::processChild(TiXmlElement* children_element, string& parent) {
	TiXmlElement* child_element = (TiXmlElement*) children_element->FirstChild("child");
	
	while (child_element != NULL) {
		string node_name = child_element->FirstChildElement("name")->GetText();
		game_manager->getRenderManager()->createSceneNode(node_name, parent);
		
        processAnimations(child_element, node_name);

		processEntity(child_element, node_name);
		
		//process translate
		TiXmlElement* translate_element = child_element->FirstChildElement("translation");
		if (translate_element != NULL) {
			string translate_str = translate_element->GetText();
			processTransformations(TRANSLATE, translate_str, node_name);
		}
		
		//process rotate
		TiXmlElement* rotate_element = child_element->FirstChildElement("rotation");
		if (rotate_element != NULL) {
			string rotate_str = rotate_element->GetText();
			processTransformations(ROTATE, rotate_str, node_name);
		}

		//process scale
		TiXmlElement* scale_element = child_element->FirstChildElement("scale");
		if (scale_element != NULL) {
			string scale_str = scale_element->GetText();
			processTransformations(SCALE, scale_str, node_name);
		}
		
		//recurse if more
		TiXmlElement* grand_children_element = child_element->FirstChildElement("children");
		if (grand_children_element != NULL)
			processChild(grand_children_element, node_name);

		child_element = child_element->NextSiblingElement();
	}
}

void ParseXML::processAnimations(TiXmlElement* child_element, string& parent) {
    TiXmlElement* animation_element = child_element->FirstChildElement("animation");
    if (animation_element != NULL) {
        string name = animation_element->FirstChildElement("name")->GetText();
        string seconds = animation_element->FirstChildElement("seconds")->GetText();
        game_manager->getRenderManager()->createAnimation(name, Utils::parseFloat(seconds), parent);

        TiXmlElement* keyframes_element = animation_element->FirstChildElement("keyframes");
        TiXmlElement* keyframe_element = keyframes_element->FirstChildElement("keyframe");
        int key_frame_count = 0;
        while (keyframe_element != NULL) {
            string time = keyframe_element->FirstChildElement("time")->GetText();
            game_manager->getRenderManager()->createAnimationKeyFrame(name, Utils::parseFloat(time));

            //process translate
            TiXmlElement* translate_element = keyframe_element->FirstChildElement("translation");
            if (translate_element != NULL) {
                string translate_str = translate_element->GetText();
                processTransformations(TRANSLATE, translate_str, name, key_frame_count);
            }
            
            //process rotate
            TiXmlElement* rotate_element = keyframe_element->FirstChildElement("rotation");
            if (rotate_element != NULL) {
                string rotate_str = rotate_element->GetText();
                processTransformations(ROTATE, rotate_str, name, key_frame_count);
            }

            //process scale
            TiXmlElement* scale_element = keyframe_element->FirstChildElement("scale");
            if (scale_element != NULL) {
                string scale_str = scale_element->GetText();
                processTransformations(SCALE, scale_str, name, key_frame_count);
            }

            key_frame_count++;
            keyframe_element = keyframe_element->NextSiblingElement();
        }

        game_manager->getRenderManager()->createAnimationState(name);
    }
}

void ParseXML::processEntity(TiXmlElement* child_element, string& scene_node) {
	TiXmlElement* entity_element = child_element->FirstChildElement("entity");
	if (entity_element != NULL) {
		string entity_name = child_element->FirstChildElement("name")->GetText();
		string entity_mesh = entity_element->FirstChildElement("mesh")->GetText();
		string entity_material = entity_element->FirstChildElement("material")->GetText();

		game_manager->getRenderManager()->createEntity(entity_name, entity_mesh, entity_material, scene_node);
	}
}

void ParseXML::processTransformations(TransformType transform_type, string& transform_str, string& scene_node) {
	float* transform_values = new float[4];
	Utils::parseFloats(transform_str, transform_values);
	
    RenderManager* rm = game_manager->getRenderManager();
	switch (transform_type) {
		case TRANSLATE: rm->processTranslate(scene_node, transform_values);
				        break;
		case ROTATE: 	rm->processRotate(scene_node, transform_values);						
						break;
		case SCALE: 	rm->processScale(scene_node, transform_values);
						break;
	}
	
	delete[] transform_values;
}

void ParseXML::processTransformations(TransformType transform_type, string& transform_str, string& animation_name, int frame_index) {
    float* transform_values = new float[4];
    Utils::parseFloats(transform_str, transform_values);
    
    RenderManager* rm = game_manager->getRenderManager();
    switch (transform_type) {
        case TRANSLATE: rm->processAnimationTranslate(animation_name, frame_index, transform_values);
                        break;
        case ROTATE:    rm->processAnimationRotate(animation_name, frame_index, transform_values);
                        break;
        case SCALE:     rm->processAnimationScale(animation_name, frame_index, transform_values);
                        break;
    }
    
    delete[] transform_values;
}

void ParseXML::parseScenePhysics(const char* level) {
	string file_name(level);
	string path = level_name + "_physics.xml";
	TiXmlDocument doc(path.c_str());
	if (!doc.LoadFile()) return;
	
	TiXmlElement* physics_element = doc.RootElement();
	string gravity_str = physics_element->FirstChildElement("gravity")->GetText();
	float* gravity = new float[3];
	Utils::parseFloats(gravity_str, gravity);
	game_manager->getPhysicsManager()->setGravity(gravity);

	string debug_str = physics_element->FirstChildElement("debug")->GetText();
	int debug_mode = Utils::parseInt(debug_str);
	game_manager->getPhysicsManager()->setDebugMode(debug_mode);

	float* translation = new float[3];
	float* rotation = new float[4];
	float* scale = new float[3];
	
	TiXmlElement* object_element = physics_element->FirstChildElement("object");
	while (object_element != NULL) {
		string node = object_element->FirstChildElement("node")->GetText();
		string shape = object_element->FirstChildElement("shape")->GetText();

		string mass_str = object_element->FirstChildElement("mass")->GetText();
		float mass = Utils::parseFloat(mass_str);

		string translation_str = object_element->FirstChildElement("translation")->GetText();
		Utils::parseFloats(translation_str, translation);

		string rotation_str = object_element->FirstChildElement("rotation")->GetText();
		Utils::parseFloats(rotation_str, rotation);

		string scale_str = object_element->FirstChildElement("scale")->GetText();
		Utils::parseFloats(scale_str, scale);

		game_manager->getPhysicsManager()->createCollisionShape(node, shape, scale, translation, rotation, mass);
		object_element = object_element->NextSiblingElement();
	}

	game_manager->getPhysicsManager()->createRigidBodies();

	delete[] translation;
	delete[] rotation;
	delete[] scale;
	delete[] gravity;
}