#include <vector>
#include <map>
#include <iostream>

#include "AudioManager.h"
#include "BassAudioManager.h"
#include "AudioResource.h"
#include "AudioPlayer.h"
#include "GameManager.h"

using namespace std;

struct AudioResourceInfo {
	HSAMPLE sample_data;
	HSAMPLE stream_data;
	HSAMPLE channel_data;
};

BassAudioManager::BassAudioManager(GameManager* game_manager) : AudioManager(game_manager) {
	audio_players = new vector<AudioPlayer*>();
	loadedAudio = new map<string, AudioResource*>();
	init();
}

BassAudioManager::~BassAudioManager() {
	BASS_Free();

	for (int i = 0; i < audio_players->size(); i++) {
		AudioPlayer* player = audio_players->at(i);
		delete player;
	}

	map<string, AudioResource*>::iterator iter;
	for (iter = loadedAudio->begin(); iter != loadedAudio->end(); iter++) {
		AudioResource* resource = iter->second;
		delete resource;
	}

	delete audio_players;
	delete loadedAudio;
	audio_players = NULL;
	loadedAudio = NULL;
}

void BassAudioManager::init(int Device, DWORD SampleRate, DWORD flags, HWND win) {
	BOOL bassActive = BASS_Init(Device, SampleRate, flags, win, NULL);
	if (!bassActive)
		game_manager->logProblem("Problem initializing Audio Manager", "BassAudioManager.cpp", 48);

	stringstream DeviceStringStream;
	if (BASS_GetDeviceInfo(Device, &device_info)) {
		DeviceStringStream << "Audio Device Info. Name: " << device_info.name << "Driver: ";
		device_info_str = DeviceStringStream.str();
	} else {
		BASS_Free();
		game_manager->logProblem("Problem initializing Audio Manager", "BassAudioManager.cpp", 56);
	}
}

AudioResourceInfo* BassAudioManager::createAudioResourceInfo() {
	AudioResourceInfo* ar_info = (AudioResourceInfo*) malloc(sizeof(AudioResourceInfo));
	ar_info->sample_data = 0;
	ar_info->channel_data = 0;
	return ar_info;
}

void BassAudioManager::loadResources(vector<AudioResource*>* resources) {
	for (int i = 0; i < resources->size(); i++) {
		bool success;
		AudioResource* resource = resources->at(i);
		resource->setInfo(createAudioResourceInfo());

		if (resource->getType() == SAMPLE)
			success = loadSampleAudioResource(resource->getFilePath(), resource->getInfo());
		else
			success = loadStreamAudioResource(resource->getFilePath(), resource->getInfo());
		
		if (success) {
			string name = resource->getName();
			loadedAudio->insert(std::pair<string, AudioResource*>(name, resource));
		} else
			free(resource->getInfo());
	}
}

bool BassAudioManager::loadSampleAudioResource(string& file_name, AudioResourceInfo* info) {
	bool success = true;
	info->sample_data = BASS_SampleLoad(FALSE, file_name.c_str(), 0, 0, 1, 0);
	if (info->sample_data)
		info->channel_data = BASS_SampleGetChannel(info->sample_data, false);
	else {
		game_manager->logProblem("Sample audio initialization error", "BassAudioManager.cpp", 92);
		success = false;
	}

	return success;
}

bool BassAudioManager::loadStreamAudioResource(string& file_name, AudioResourceInfo* info) {
	bool success = true;
	info->stream_data = BASS_StreamCreateFile(FALSE, file_name.c_str(), 0, 0, 0);
	if (info->stream_data)
		info->channel_data = info->stream_data;
	else {
		game_manager->logProblem("Stream audio initialization error", "BassAudioManager.cpp", 105);
		success = false;
	}
		
	return success;
}

void BassAudioManager::unloadResources(vector<AudioResource*>* resources) {
	for (int i = 0; i < resources->size(); i++) {
		AudioType type = resources->at(i)->getType();
		if (type == SAMPLE)
			unloadSampleAudioResource(resources->at(i)->getInfo());
		else
			unloadStreamAudioResource(resources->at(i)->getInfo());
		free(resources->at(i)->getInfo());
	}

	//assuming unloading the whole level
	loadedAudio->clear();
}

void BassAudioManager::unloadSampleAudioResource(AudioResourceInfo* info) {
	if (BASS_ChannelIsActive(info->channel_data) == BASS_ACTIVE_STOPPED)
		BASS_ChannelStop(info->channel_data);

	BASS_SampleFree(info->sample_data);
	info->sample_data = 0;
	info->channel_data = 0;
}

void BassAudioManager::unloadStreamAudioResource(AudioResourceInfo* info) {
	if (!BASS_ChannelIsActive(info->channel_data) == BASS_ACTIVE_STOPPED) 
		BASS_ChannelStop(info->channel_data);

	BASS_StreamFree(info->stream_data);
	info->stream_data = 0;
	info->channel_data = 0;
}

void BassAudioManager::playAudio(const char* audio_name, uint32 num_repeats, bool restart) {
	AudioResource* audio = (*loadedAudio)[string(audio_name)];
	AudioPlayer* player = new AudioPlayer(audio, num_repeats);
	// addAudioPlayer(player);
	audio_players->push_back(player);
	AudioResourceInfo* info = player->getAudioResource()->getInfo();
	BASS_ChannelPlay(info->channel_data, restart);
}

void BassAudioManager::updateAudio(float time_step) {
	int num_players = audio_players->size();
	for (int i = 0; i < num_players; i++) {
		AudioPlayer* player = audio_players->at(i);
		AudioResource* audio = player->getAudioResource();
		AudioResourceInfo* info = audio->getInfo();

		if (BASS_ChannelIsActive(info->channel_data) == BASS_ACTIVE_STOPPED) {
			if (player->getRepeatCount() >= player->getNumRepeats()) {
				audio_players->erase(audio_players->begin() + i);
				delete player;
				
				i--;
				num_players--;
			} else {
				player->incRepeatCount();
				BASS_ChannelPlay(info->channel_data, false);
			}
		}
	}
}

void BassAudioManager::addAudioPlayer(AudioPlayer* player) {
	
}

void BassAudioManager::setVolume(float volume) {
	BASS_SetVolume(volume);
}

void BassAudioManager::pauseAudio(const char* audio_name) {
	AudioResource* resource = (*loadedAudio)[string(audio_name)];
	AudioResourceInfo* info = resource->getInfo();
	BASS_ChannelPause(info->channel_data);
	BASS_ChannelSetPosition(info->channel_data, 0, BASS_POS_BYTE);
}

void BassAudioManager::pauseAll() {
	BASS_Pause();
}

void BassAudioManager::start() {
	BASS_Start();
}

bool BassAudioManager::isAudioPlaying(const char* audio_name) {
	AudioResource* resource = (*loadedAudio)[string(audio_name)];
	AudioResourceInfo* info = resource->getInfo();
	return BASS_ChannelIsActive(info->channel_data) == BASS_ACTIVE_PLAYING;
}