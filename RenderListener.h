#if !defined RENDER_LISTENER
#define RENDER_LISTENER

#include "Ogre.h"
class RenderManager;

class RenderListener : public Ogre::FrameListener {
	
	private:
		RenderManager* render_manager;
		bool render;

	protected:
		RenderManager* getRenderManager();

	public:
		RenderListener(RenderManager* render_manager);
		virtual ~RenderListener();

		virtual bool frameStarted(const::Ogre::FrameEvent& event) {
			return getRenderStatus();
		}

		virtual bool frameRenderingQueued(const Ogre::FrameEvent& event) {
			return getRenderStatus();
		}

		virtual bool frameEnded(const Ogre::FrameEvent& event) {
			return getRenderStatus();
		}

        void startRendering();
		void stopRendering();
		bool getRenderStatus();
};

#endif