#if !defined LEVEL_H
#define LEVEL_H

using namespace std;

class LevelResources;

class Level {
	private:
		const char* name;
		LevelResources* resources;

	public:
		Level(const char* name, LevelResources* resources);
		~Level();

		const char* getName();
		LevelResources* getResources();		
};

#endif