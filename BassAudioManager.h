#ifndef BASS_AUDIO_MANAGER_H
#define BASS_AUDIO_MANAGER_H

#include <sstream>
#include <vector>

#include "GameHeader.h"
#include "AudioManager.h"
#include "bass.h"

class AudioPlayer;
class AudioResource;
struct AudioResourceInfo;

class BassAudioManager : public AudioManager {
	private:
		std::vector<AudioPlayer*>* audio_players;
		std::map<std::string, AudioResource*>* loadedAudio;

		BASS_DEVICEINFO device_info;
		std::string device_info_str;

		bool loadSampleAudioResource(std::string& file_name, AudioResourceInfo* info);
		bool loadStreamAudioResource(std::string& file_name, AudioResourceInfo* info);
		void unloadSampleAudioResource(AudioResourceInfo* info);
		void unloadStreamAudioResource(AudioResourceInfo* info);

	public:
		BassAudioManager(GameManager* game_manager);
		virtual ~BassAudioManager();

		AudioResourceInfo* createAudioResourceInfo();
		void init(int Device = 1, DWORD SampleRate = 44100, DWORD flags = 0, HWND win = 0);

		void setVolume(float volume);
		void pauseAll();
		void start();

		void addAudioPlayer(AudioPlayer* audio_player);

		void updateAudio(float time_step);
		void playAudio(const char* audio_name, uint32 num_repeats, bool restart = false);
		void pauseAudio(const char* audio_name);

		bool isAudioPlaying(const char* audio_name);

		void loadResources(std::vector<AudioResource*>* resources);
		void unloadResources(std::vector<AudioResource*>* resources);
};

#endif