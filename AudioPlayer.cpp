#include "AudioPlayer.h"

AudioPlayer::AudioPlayer(AudioResource* audio, int num_plays) {
	this->audio = audio;
	this->num_repeats = num_plays - 1;
	this->repeat_count = 0;
}

AudioPlayer::~AudioPlayer() {
	audio = 0;
}

int AudioPlayer::getNumRepeats() {
	return num_repeats;
}

int AudioPlayer::getRepeatCount() {
	return repeat_count;
}

AudioResource* AudioPlayer::getAudioResource() {
	return audio;
}

void AudioPlayer::incRepeatCount() {
	repeat_count++;
}