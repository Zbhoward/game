#ifndef CEGUI_MANAGER_H
#define CEGUI_MANAGER_H

#include "GameHeader.h"
#include "GameManager.h"
#include "GUIManager.h"
#include "InputManager.h"
#include "CEGUI/CEGUI.h"
#include "CEGUI/RendererModules/Ogre/Renderer.h"

#include <map>

class RenderManager;

class CEGUIManager : public GUIManager {
	private:
		// RenderManager* render_manager;
		CEGUI::OgreRenderer* cegui_ogre_renderer;

		CEGUI::GUIContext* gui_context;
		CEGUI::Window* root_window;

        map<std::string, std::string>* events;

		static CEGUI::OgreRenderer* getRenderer();
        void createGUIContext(string& scheme, string& font, string& cursor, string& tooltip, string& layout);
        void destroyGUIContext();

        string getFullName(CEGUI::Window* window);

        CEGUI::Key::Scan specialKeyMap(Key key);

	public:
		CEGUIManager(GameManager* GameManager);
		virtual ~CEGUIManager();

        void initializeGroups(string& level);
		void loadGUI(string& level, string& scheme, string& font, string& cursor, string& tooltip, string& layout);
        void unloadGUI();

        bool isElementActive(string element);

        void buttonEvent(const CEGUI::EventArgs& e);
        void scrollEvent(const CEGUI::EventArgs& e);
        void mousePressedEvent(const CEGUI::EventArgs& e);

		void keyPressed(Key key);
		void mouseMoved(uint32 mouse_x, uint32 mouse_y, int mouse_rel_x, int mouse_rel_y);
        void mousePressed(uint32 x_click, uint32 y_click, uint32 button);

        void processEvent(std::string& type, std::string& name, std::string& script);
};

#endif