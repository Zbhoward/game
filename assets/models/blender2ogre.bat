@echo off
cls

set DRIVE_LETTER=C:
set MESH=%1
set GAME_DIR=%DRIVE_LETTER%/Users/howar/Desktop/GEP/Engine

set OGRE_PATH=%GAME_DIR%\ogre_src_v1-8-1\build\bin
set ZLIB_DIR=%GAME_DIR%/zlib-1.2.11
set FREEIMAGE_DIR=%GAME_DIR%/FreeImage
set PATH=C:/Windows;%OGRE_PATH%;%FREEIMAGE_DIR%/lib;%ZLIB_DIR%/lib;

OgreXMLConverter %MESH%.xml
OgreMeshUpgrader %MESH%

del OgreXMLConverter.log
del OgreMeshUpgrade.log