StargateAnim = {}
StargateAnim.__index = StargateAnim

function create()
	print("creating object...")
    local stargate_anim = {}           	 	 -- our new object
    setmetatable(stargate_anim,StargateAnim)  -- make Account handle lookup

    stargate_anim.state = 0
    stargate_anim.address = "B"      -- initialize our object
    stargate_anim.degrees_left = 0
    stargate_anim.curr_index = 0
    stargate_anim.curr_char = "0"
    stargate_anim.chevron_playing = false

    return stargate_anim
end

anim = create()

function dial_onEvent(address)
	if anim.state ~= 0 then 
		return 
	end

    anim.address = address
    print("Address: ", anim.address)
    anim.curr_index = 1
    local next_char = address:sub(1,1)
    updateDegreesLeft(anim.curr_char, next_char)
    anim.state = 1
end

function dial_update()
	if anim.state == 1 then	updateRotation()
	elseif anim.state == 2 then updateChevron()
	end
end

function updateDegreesLeft(start_char, end_char)
	local chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ@$?"
	local start = chars:find(start_char) - 1
	local _end = chars:find(end_char) - 1

	local distance = 0
	if start > _end then
		distance = start - _end
		if anim.curr_index % 2 ~= 0 then
			distance = 39 - distance
		end
	elseif _end > start then
		distance = _end - start
		if anim.curr_index % 2 == 0 then
			distance = 39 - distance
		end
	else
		distance = 39
	end

	anim.degrees_left = (distance * (120.0 / 13.0))
	anim.curr_char = end_char

	print("Starting char: ", start_char)
	print("Ending char:", end_char)
	print("start: ", start, " end: ", _end, " degrees: ", anim.degrees_left, " distance: ", distance) 
end

function updateRotation()
	local degree = .06
	if anim.degrees_left <= 0.0 then
		anim.state = 2
		audio_manager:pauseAudio("Ring roll")
		return
	end

	if anim.degrees_left - degree < 0 then
		degree = anim.degrees_left
	end

	anim.degrees_left = anim.degrees_left - degree

	if anim.curr_index % 2 == 0 then
		degree = degree * -1
	end

	render_manager:rotateMesh("Ring Rotate", degree, 0, 0, 1)

	if not audio_manager:isAudioPlaying("Ring roll") then
		audio_manager:playAudio("Ring roll", 1, true)
	end
end

function updateChevron()
	if not audio_manager:isAudioPlaying("Chevron lock") then
		if anim.chevron_playing then
			anim.chevron_playing = false

			if anim.curr_index == 7 then
				anim.state = 0
				audio_manager:playAudio("Lock fail", 1)
			else
				anim.curr_index = anim.curr_index + 1
				local next_char = anim.address:sub(anim.curr_index, anim.curr_index)
				updateDegreesLeft(anim.curr_char, next_char)
				anim.state = 1
			end

			return
		end

		render_manager:startAnimation("Inner Lock")
		render_manager:startAnimation("Outer Lock")
		audio_manager:playAudio("Chevron lock", 1)
		anim.chevron_playing = true
	end
end

function isPlaying()
	return anim.state ~= 0
end
