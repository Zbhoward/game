function keyPressed(key)
	print(key)
	if key == "/" then
		render_manager:startAnimation("Inner Lock")
		render_manager:startAnimation("Outer Lock")
		audio_manager:playAudio("Chevron lock", 1)
    elseif key == "W" then
    	physics_manager:applyImpulse("Stargate Node", 0, 0, -10)
    elseif key == "S" then 
    	physics_manager:applyImpulse("Stargate Node", 0, 0, 10)
    elseif key == "A" then 
    	physics_manager:applyImpulse("Stargate Node", -10, 0, 0)
    elseif key == "D" then	 
    	physics_manager:applyImpulse("Stargate Node", 10, 0, 0)
    elseif key == " " then 
    	physics_manager:applyImpulse("Stargate Node", 0, 50, 0)
    end
end

function leftAxisMoved(north_south, east_west)
	physics_manager:applyImpulse("Stargate Node", east_west*.5, 0, north_south*.5)
end

function rightAxisMoved(north_south, east_west)
	physics_manager:applyTorqueImpulse("Stargate Node", north_south*.4, east_west*.4, 0)
end

function buttonPressed(button)
	if button == "A" then
		physics_manager:applyImpulse("Stargate Node", 0, 50, 0)
	end
end