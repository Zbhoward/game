#ifndef AUDIO_RESOURCE_H
#define AUDIO_RESOURCE_H

#include <string>

struct AudioResourceInfo;
enum AudioType { STREAM, SAMPLE };

class AudioResource {
	private:
		std::string name;
		std::string file_path;
		AudioResourceInfo* info;
		AudioType type;

	public:
		AudioResource(std::string& name, std::string& file_path, std::string& type);
		~AudioResource();

		std::string& getName();
		std::string& getFilePath();
		AudioResourceInfo* getInfo();
		AudioType getType();

		void setInfo(AudioResourceInfo* info);
};

#endif