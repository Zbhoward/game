#ifndef OIS_INPUT_MANAGER_H
#define OIS_INPUT_MANAGER_H

#include "GameHeader.h"
#include "InputManager.h"

#include <OIS.h>
#include <vector>
#include <iostream>

class OISInputManager : public InputManager, public OIS::KeyListener, public OIS::MouseListener, public OIS::JoyStickListener {
    private:
        OIS::InputManager* ois_input_manager;

        OIS::Keyboard* keyboard;
        OIS::Mouse* mouse;
        OIS::JoyStick* joystick;

        bool left_axis_moved;

        int window_width;
        int window_height;

        void init();
        void free();

        Key keyMap(const OIS::KeyEvent& e);
        uint32 mouseMap(const OIS::MouseButtonID id);
        std::string joystickButtonMap(int button);

    public:
        OISInputManager(GameManager* gm);
        ~OISInputManager();

        void checkForInput();    

        bool keyPressed(const OIS::KeyEvent& e);
        bool keyReleased(const OIS::KeyEvent& e);

        bool mouseMoved(const OIS::MouseEvent& e);
        bool mousePressed(const OIS::MouseEvent& e, OIS::MouseButtonID button);
        bool mouseReleased(const OIS::MouseEvent& e, OIS::MouseButtonID button);

        bool buttonPressed(const OIS::JoyStickEvent& e, int button);
        bool buttonReleased(const OIS::JoyStickEvent& e, int button);
        bool sliderMoved(const OIS::JoyStickEvent& e, int index);
        bool povMoved(const OIS::JoyStickEvent& e, int index);
        bool vector3Moved(const OIS::JoyStickEvent& e, int index);
        bool axisMoved(const OIS::JoyStickEvent& e, int axis);
};

#endif