#if !defined PARSE_XML_H
#define PARSE_XML_H

#include <string>

#include "tinyxml.h"
#include "Level.h"

using namespace std;

class GameManager;

class ParseXML {
	
	private:
		enum TransformType { TRANSLATE, ROTATE, SCALE };

		GameManager* game_manager;
		string level_name;

		void processCamera(TiXmlElement* camera_element);
		void processLights(TiXmlElement* lights_element);
		void processScripts(TiXmlElement* scripts_element);
		void processGUI(TiXmlElement* gui_element);
		void processChild(TiXmlElement* children_element, string& parent);
		void processAnimations(TiXmlElement* child_element, string& parent);
		void processEntity(TiXmlElement* child_element, string& scene_node);
		void processTransformations(TransformType transform_type, string& transform_str, string& scene_node);
		void processTransformations(TransformType transform_type, string& transform_str, string& animation_name, int frame_index);

	public:
		ParseXML(GameManager* game_manager);

		void parseSceneXML(const char* file_name);
		void parseScenePhysics(const char* file_name);

		static std::map<string, Level*>* parseResourceXML(const char* file_name);		
};

#endif