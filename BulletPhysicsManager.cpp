#include "BulletPhysicsManager.h"
#include "RenderManager.h"

#include "BulletSceneNodeMotionState.h"
#include "BulletDebugDrawer.h"

#include "RigidBody.h"
#include "CompoundShape.h"
#include "CompareRigidBodies.h"
#include "CompareCompoundShapes.h"

#include <iostream>

BulletPhysicsManager::BulletPhysicsManager(RenderManager* rm) : PhysicsManager(rm) {
	CompareRigidBodies* compare_rigid_bodies = new CompareRigidBodies();
	rigid_bodies = new TableAVL<RigidBody, string>(compare_rigid_bodies);
	CompareCompoundShapes* compare_compound_shapes = new CompareCompoundShapes();
	compound_shapes = new TableAVL<CompoundShape, string>(compare_compound_shapes);

	init();

	BulletDebugDrawer* debug_drawer = new BulletDebugDrawer(rm);
	dynamics_world->setDebugDrawer(debug_drawer);
}

BulletPhysicsManager::~BulletPhysicsManager() {
	AVLTreeIterator<CompoundShape>* compound_shape_iter = compound_shapes->tableIterator();
	while (compound_shape_iter->hasNext()) {
		CompoundShape* cs = compound_shape_iter->next();
		delete cs;
	}
	delete compound_shape_iter;
	delete compound_shapes;

	int num_collision_objects = dynamics_world->getNumCollisionObjects();
	btAlignedObjectArray<btCollisionObject*> rigid_bodies_array = dynamics_world->getCollisionObjectArray();
	for (int i = num_collision_objects - 1; i >= 0; i--) {
		btRigidBody* rigid_body = btRigidBody::upcast(rigid_bodies_array[i]);
		dynamics_world->removeCollisionObject(rigid_body);

		delete rigid_body->getMotionState();
		delete rigid_body->getCollisionShape();
	}

	AVLTreeIterator<RigidBody>* rigid_body_iter = rigid_bodies->tableIterator();
	while (rigid_body_iter->hasNext()) {
		RigidBody* rb = rigid_body_iter->next();
		delete rb;
	}
	delete rigid_body_iter;
	delete rigid_bodies;

	BulletDebugDrawer* debug_drawer = (BulletDebugDrawer*) dynamics_world->getDebugDrawer();
	delete debug_drawer;

	delete dynamics_world;
	delete solver;
	delete dispatcher;
	delete collision_configuration;
	delete overlapping_pair_cache;
}

void BulletPhysicsManager::init() {
	overlapping_pair_cache = new btDbvtBroadphase();
	collision_configuration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collision_configuration);
	solver = new btSequentialImpulseConstraintSolver();
	dynamics_world = new btDiscreteDynamicsWorld(dispatcher, overlapping_pair_cache, solver, collision_configuration);
}

void BulletPhysicsManager::createRigidBodies() {
	AVLTreeIterator<CompoundShape>* iter = compound_shapes->tableIterator();
	btCompoundShape* com_shape = 0;

	while (iter->hasNext()) {
		CompoundShape* cs = iter->next();

		SceneNodeMotion* snm = render_manager->createSceneNodeMotion(*(cs->getKey()));
		BulletSceneNodeMotionState* ms = new BulletSceneNodeMotionState(cs->getKey(), snm, render_manager);

		btScalar* m = new btScalar(cs->getMass());
		com_shape = cs->getCompoundShape();
		btVector3* li = new btVector3(0, 0, 0);

		com_shape->calculateLocalInertia(*m, *li);
		btRigidBody::btRigidBodyConstructionInfo* rbci = new btRigidBody::btRigidBodyConstructionInfo(*m, ms, com_shape, *li);
		btRigidBody* bt_rb = new btRigidBody(*rbci);
		bt_rb->setDamping(.1, .1);
		bt_rb->setActivationState(DISABLE_DEACTIVATION);
		RigidBody* rb = new RigidBody(*(cs->getKey()), bt_rb);
		rigid_bodies->tableInsert(rb);

		dynamics_world->addRigidBody(bt_rb);
	}
	delete iter;
}

void BulletPhysicsManager::createCollisionShape(string& compound_shape_id, string& collision_shape, float* params, 
	float* translation, float* rotation, float mass) {
	btCollisionShape* col_shape = NULL;
	if (collision_shape == "sphere")
		col_shape = new btSphereShape(btScalar(params[0]));
	else if (collision_shape == "cylinder_y")
		col_shape = new btCylinderShape(btVector3(params[0], params[1], params[2]));
	else if (collision_shape == "cylinder_z")
		col_shape = new btCylinderShapeZ(btVector3(params[0], params[1], params[2]));
	else
		col_shape = new btBoxShape(btVector3(params[0], params[1], params[2]));

	btTransform* tr = new btTransform();
	tr->setIdentity();

	tr->setOrigin(btVector3(translation[0], translation[1], translation[2]));
	tr->setRotation(btQuaternion(rotation[0], rotation[1], rotation[2], rotation[3]));

	CompoundShape* cs = compound_shapes->tableRetrieve(&compound_shape_id);
	if (cs == NULL) {
		compound_shapes->tableInsert(new CompoundShape(compound_shape_id, new btCompoundShape));
		cs = compound_shapes->tableRetrieve(&compound_shape_id);
	}

	cs->addChildShape(tr, col_shape, mass);
}

void BulletPhysicsManager::updateRigidBodies() {
	int num_collision_objects = dynamics_world->getNumCollisionObjects();
	btAlignedObjectArray<btCollisionObject*> bodies = dynamics_world->getCollisionObjectArray();

	for (int i = num_collision_objects - 1; i >= 0; i--) {
		btRigidBody* rigid_body = btRigidBody::upcast(bodies[i]);
		BulletSceneNodeMotionState* motion_state = (BulletSceneNodeMotionState*) rigid_body->getMotionState();

		btTransform trans;
		motion_state->copyNodeTransformIntoBulletTransform();
		motion_state->getWorldTransform(trans);

		btQuaternion orient = trans.getRotation();
		SceneNodeMotion* snm = motion_state->getSceneNodeMotion();
		render_manager->setPosition(snm, trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ());
		render_manager->setOrientation(snm, orient.getW(), orient.getX(), orient.getY(), orient.getZ());
	}
}

void BulletPhysicsManager::stepPhysicsSimulation(float time_incr) {
	btScalar time_step(time_incr);
	// updateRigidBodies();
	// updateRigidBodyVelocity(time_incr);
	dynamics_world->stepSimulation(time_step, 10, .01667);
	dynamics_world->debugDrawWorld();
}

void BulletPhysicsManager::updateRigidBodyVelocity(float time_incr) {
	AVLTreeIterator<RigidBody>* rigid_body_iter = rigid_bodies->tableIterator();
	while (rigid_body_iter->hasNext()) {
		RigidBody* rb = rigid_body_iter->next();
		rb->incrementCustomVelocity(time_incr);
	}
	delete rigid_body_iter;
}

void BulletPhysicsManager::setDebugMode(int debug) {
	dynamics_world->getDebugDrawer()->setDebugMode(debug);
}

void BulletPhysicsManager::setGravity(float* g) {
	dynamics_world->setGravity(btVector3(g[0], g[1], g[2]));
}

void BulletPhysicsManager::getGravity(float* g) {
	btVector3 gravity = dynamics_world->getGravity();
	g[0] = gravity.x();
	g[1] = gravity.y();
	g[2] = gravity.z();
}

void BulletPhysicsManager::getLinearVelocity(string rigid_body_name, float* v) {
	RigidBody* rb = rigid_bodies->tableRetrieve(&rigid_body_name);

	if (rb) {
		btRigidBody* bt_rb = rb->getRigidBody();
		btVector3 lienar_velocity = bt_rb->getLinearVelocity();
		v[0] = lienar_velocity.x();
		v[1] = lienar_velocity.y();
		v[2] = lienar_velocity.z();
	}
}

void BulletPhysicsManager::setRigidBodyVelocity(string& rigid_body_name, float end_time, float* a) {
	RigidBody* rb = rigid_bodies->tableRetrieve(&rigid_body_name);
	rb->setCustomVelocity(end_time, a);
}

void BulletPhysicsManager::applyImpulse(const char* rigid_body_name, float ns, float ew, float other) {
	string rigid_body_str = rigid_body_name;
	btVector3 impulse(ns, ew, other);
	btVector3 rel_pos(0, 0, 0);
	RigidBody* rb = rigid_bodies->tableRetrieve(&rigid_body_str);
	if (rb) {
		btRigidBody* bt_rb = rb->getRigidBody();
		bt_rb->applyImpulse(impulse, rel_pos);
	}
}

void BulletPhysicsManager::applyTorqueImpulse(const char* rigid_body_name, float pitch, float yaw, float roll) {
	string rigid_body_str = rigid_body_name;
	RigidBody* rb = rigid_bodies->tableRetrieve(&rigid_body_str);
	if (rb) {
		btRigidBody* bt_rb = rb->getRigidBody();
		bt_rb->applyTorqueImpulse(btVector3(pitch, yaw, roll));
	}
}
