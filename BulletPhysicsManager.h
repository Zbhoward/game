#ifndef BULLET_PHYSICS_MANAGER_H
#define BULLET_PHYSICS_MANAGER_H

#include "btBulletDynamicsCommon.h"
#include "TableAVL.h"
#include "PhysicsManager.h"

class RigidBody;
class CompoundShape;
class RenderManager;
struct SceneNodeMotion;

class BulletPhysicsManager : public PhysicsManager {
	private:
		// RenderManager* render_manager;
		TableAVL<RigidBody, std::string>* rigid_bodies;
		TableAVL<CompoundShape, std::string>* compound_shapes;

		btBroadphaseInterface* overlapping_pair_cache;
		btDefaultCollisionConfiguration* collision_configuration;
		btCollisionDispatcher* dispatcher;

		btSequentialImpulseConstraintSolver* solver;
		btDiscreteDynamicsWorld* dynamics_world;

		void init();

	public:
		BulletPhysicsManager(RenderManager* render_manager);
		virtual ~BulletPhysicsManager();

		void createRigidBodies();
		void updateRigidBodies();
		void updateRigidBodyVelocity(float time_incr);
		void setRigidBodyVelocity(string& rigid_body_name, float end_time, float* a);

		void createCollisionShape(string& compound_shape_id, string& collision_shape, float* params, 
			float* translation, float* rotation, float mass);

		void stepPhysicsSimulation(float time_incr);

		void setDebugMode(int debug);
		void setGravity(float* g);
		void getGravity(float* g);

		void getLinearVelocity(string rigid_body_name, float* v);

		void applyImpulse(const char* rigid_body_name, float ns, float ew, float other);
		void applyTorqueImpulse(const char* rigid_body_name, float pitch, float yaw, float roll);
};

#endif