#include <vector>
#include <map>

#include "ResourceManager.h"
#include "ParseXML.h"
#include "LevelResources.h"
#include "GameManager.h"
#include "GameHeader.h"

using namespace std;

ResourceManager::ResourceManager(const char* resource_file, GameManager* game_manager) {
	gm = game_manager;
	game_manager->logComment("Loading resources...");
	levels = ParseXML::parseResourceXML(resource_file);
	game_manager->logComment("Done loading resources.");
}

void ResourceManager::loadLevel(const char* level_name) {
	if (loaded_level == level_name)
		return;

	Level* level = levels->at(level_name);
	LevelResources* resources = level->getResources();

	gm->loadRenderingResources(level_name, resources->getPaths(), resources->getMeshes());
	gm->loadAudioResources(resources->getAudio());

	loaded_level = level_name;
}

void ResourceManager::unloadLevel(const char* level_name) {
	Level* level = levels->at(level_name);
	LevelResources* resources = level->getResources();

	gm->unloadRenderingResources(level_name);
	gm->unloadAudioResources(resources->getAudio());
}