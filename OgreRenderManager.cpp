#include <iostream>
#include <vector>

#include "OgreRenderManager.h"
#include "GameManager.h"
#include "GUIManager.h"
#include "InputManager.h"
#include "ParseXML.h"
#include "AnimationRenderListener.h"
#include "InputRenderListener.h"
#include "ScriptManager.h"
#include "PhysicsManager.h"
#include "BulletPhysicsManager.h"
#include "RenderManager.h"

using namespace std;
using namespace Ogre;

struct SceneNodeMotion {
    SceneNode* scene_node_motion;
};

struct SceneNodeManual {
    SceneNode* scene_node_manual;
};

OgreRenderManager::OgreRenderManager(GameManager* gm) : RenderManager(gm)
{
    init();

    animation_playing = false;
    animation_states = new std::vector<AnimationState*>();
    render_listeners = new std::vector<RenderListener*>();
    scripts_to_update = new std::vector<string>();

    AnimationRenderListener* anim_listener = new AnimationRenderListener(this);
    root->addFrameListener(anim_listener);
    render_listeners->push_back(anim_listener);

    InputRenderListener* input_listener = new InputRenderListener(this);
    root->addFrameListener(input_listener);
    render_listeners->push_back(input_listener);

    physics_manager = new BulletPhysicsManager(this);
}

OgreRenderManager::~OgreRenderManager()
{
    stopRendering();

    for (int i = 0; i < render_listeners->size(); i++) {
        RenderListener* listener = render_listeners->at(i);
        delete listener;
        listener = NULL;
    }
    delete render_listeners;

    delete animation_states;    
    delete scripts_to_update;

    scene_manager->clearScene();
    scene_manager->destroyAllCameras();

    window->removeAllViewports();

    window->destroy();
    window = NULL;

    delete root;
    root = NULL;
}

void OgreRenderManager::init()
{
    root = NULL;
    window = NULL;
    scene_manager = NULL;

    root = OGRE_NEW Root("","");  //resource/config files go here
    root->loadPlugin("RenderSystem_GL");  //prepares external dlls for later use

    RenderSystem* render_system = root->getRenderSystemByName("OpenGL Rendering Subsystem"); //just returns a pointer to an uninialized render system
    if (!render_system)
    {
       //ASSERT_CRITICAL(false);
    }

    root->setRenderSystem(render_system);
    //manually set configuration options
    render_system->setConfigOption("Full Screen", "No");
    render_system->setConfigOption("Video Mode", "1920 x 1080 @ 32-bit colour");

    //initialize render system
    //automatically create the window and give it a title
    window = root->initialise(true, "Game Engine Programming");

    scene_manager = root->createSceneManager(ST_GENERIC, "Default Scene Manager");
    window->getCustomAttribute("WINDOW", &window_handle);

    //the Ogre viewport corresponds to a clipping region into which the contents of the camera view will be rendered in the window on each frame
    //by default, the size of the viewport matches the size of the window, but the viewport can be cropped
    //the camera represents a view into an existing scene and the viewport represents a region into which an existing camera will render its contents
    camera = scene_manager->createCamera("Camera");

    //z-order, left, top, width, height
    viewport = window->addViewport(camera, 0, 0.0, 0.0, 1.0, 1.0);  //assign a camera to a viewport (can have many cameras and viewports in a single window)
    viewport->setBackgroundColour(ColourValue(0,0,0));

    float actual_width = Real(viewport->getActualWidth());
    float actual_height = Real(viewport->getActualHeight());
    float aspect_ratio = actual_width/actual_height;
    camera->setAspectRatio(aspect_ratio);
}

void OgreRenderManager::createCamera(float* position, float* look_at, float* clip_distances) {
    camera->setPosition(Vector3(position[0], position[1], position[2]));
    camera->lookAt(Vector3(look_at[0], look_at[1], look_at[2]));
    camera->setNearClipDistance(clip_distances[0]);
    camera->setFarClipDistance(clip_distances[1]);
}

void OgreRenderManager::createLight(string& name, float* ambient_color, float* diffuse_color, float* direction) {
    scene_manager->setAmbientLight(ColourValue(ambient_color[0], ambient_color[1], ambient_color[2]));

    Light* light = scene_manager->createLight(name);
    light->setType(Light::LT_DIRECTIONAL);
    light->setDiffuseColour(diffuse_color[0], diffuse_color[1], diffuse_color[2]);
    light->setDirection(direction[0], direction[1], direction[2]);
}

void OgreRenderManager::createEntity(string& name, string& mesh, string& material, string& scene_node_str) {
    Entity* entity = scene_manager->createEntity(name, mesh);
    entity->setMaterialName(material);
    SceneNode* scene_node = scene_manager->getSceneNode(scene_node_str);
    scene_node->attachObject(entity);
}

void OgreRenderManager::createSceneNode(string& child, string& parent) {
    SceneNode* child_node = scene_manager->createSceneNode(child);

    SceneNode* parent_node;
    if (parent == "root")
        parent_node = scene_manager->getRootSceneNode();
    else
        parent_node = scene_manager->getSceneNode(parent);

    parent_node->addChild(child_node);
}

void OgreRenderManager::createAnimation(string& name, float length, string& scene_node) {
    SceneNode* node = scene_manager->getSceneNode(scene_node);
    Animation* animation = scene_manager->createAnimation(name, length);
    animation->setInterpolationMode(Animation::IM_SPLINE);
    animation->createNodeTrack(1, node);
}

void OgreRenderManager::createAnimationKeyFrame(string& animation_name, float time) {
    Animation* animation = scene_manager->getAnimation(animation_name);
    NodeAnimationTrack* track = animation->getNodeTrack(1);
    track->createNodeKeyFrame(time);
}

void OgreRenderManager::createAnimationState(string& animation_name) {
    AnimationState* animation_state = scene_manager->createAnimationState(animation_name);
    animation_state->setLoop(false);
    animation_states->push_back(animation_state);
}

void OgreRenderManager::processTranslate(string& scene_node, float* values) {
    SceneNode* node = scene_manager->getSceneNode(scene_node);
    node->translate(values[0], values[1], values[2], Node::TS_LOCAL);
}

void OgreRenderManager::processRotate(string& scene_node, float* values) {
    SceneNode* node = scene_manager->getSceneNode(scene_node);
    Degree angle(values[0]);
    Vector3 axis(values[1], values[2], values[3]);
    Quaternion q(angle, axis);
    node->rotate(q);
}

void OgreRenderManager::processScale(string& scene_node, float* values) {
    SceneNode* node = scene_manager->getSceneNode(scene_node);
    node->scale(values[0], values[1], values[2]);
}

void OgreRenderManager::processAnimationTranslate(string& animation_name, int frame_index, float* values) {
    TransformKeyFrame* key = getKeyFrame(animation_name, frame_index);
    key->setTranslate(Vector3(values[0], values[1], values[2]));
}

void OgreRenderManager::processAnimationRotate(string& animation_name, int frame_index, float* values) {
    TransformKeyFrame* key = getKeyFrame(animation_name, frame_index);
    Degree angle(values[0]);
    Vector3 axis(values[1], values[2], values[3]);
    Quaternion q(angle, axis);
    key->setRotation(q);
}

void OgreRenderManager::processAnimationScale(string& animation_name, int frame_index, float* values) {
    TransformKeyFrame* key = getKeyFrame(animation_name, frame_index);
    key->setScale(Vector3(values[0], values[1], values[2]));
}

TransformKeyFrame* OgreRenderManager::getKeyFrame(string& animation_name, int frame_index) {
    Animation* animation = scene_manager->getAnimation(animation_name);
    NodeAnimationTrack* track = animation->getNodeTrack(1);
    return track->getNodeKeyFrame(frame_index);
}

void OgreRenderManager::updateAnimations(float time_step) {
    bool animation_ended = false;
    for (int i = 0; i < animation_states->size(); i++) {
        AnimationState* animation_state = (*animation_states)[i];
        animation_state->addTime(time_step);
        
        //reset animations to start and then stop checking over and over and over
        animation_ended = animation_state->hasEnded();
        if (animation_playing && animation_ended) {
            animation_state->setWeight(0);
            animation_state->setEnabled(false);
        }
    }

    if (animation_playing && animation_ended)
        animation_playing = false;

    for (int i = 0; i < scripts_to_update->size(); i++) {
        string file = scripts_to_update->at(i);
        game_manager->getScriptManager()->updateCallback(file);
    }

    physics_manager->stepPhysicsSimulation(time_step);
}

void OgreRenderManager::updateEveryFrame(string script) {
    scripts_to_update->push_back(script);
}

void OgreRenderManager::loadResources(const char* level_name, std::vector<string*>* paths, std::vector<string*>* meshes) {
    ResourceGroupManager& rgm = ResourceGroupManager::getSingleton();
    string level_str = string(level_name);

    for (int i = 0; i < paths->size(); i++) {
        rgm.addResourceLocation((*paths->at(i)), "FileSystem", level_name);
    }

    for (int i = 0; i < meshes->size(); i++) {
        rgm.addResourceLocation((*meshes->at(i)), "FileSystem", level_name);
    }

    rgm.initialiseResourceGroup(level_name);
    rgm.loadResourceGroup(level_name, true, true);

    cout << "Loading scene: " << level_name << endl;
    ParseXML parser(game_manager);
    parser.parseSceneXML(level_name);
    parser.parseScenePhysics(level_name);
}

void OgreRenderManager::unloadResources(const char* level_name) {
    ResourceGroupManager& rgm = ResourceGroupManager::getSingleton();
    scene_manager->clearScene();
    rgm.destroyResourceGroup(level_name);

    game_manager->getGUIManager()->unloadGUI();
}

void OgreRenderManager::checkForInput(float time_step) {
    game_manager->getInputManager()->checkForInput();
}

// void OgreRenderManager::keyPressed(Key key) {
    // game_manager->getGUIManager()->keyPressed(key);
    // if (hasGUI)
    // if (key == "ESCAPE")
    //     stopRendering();
    // if (key == "SPACE") {
    //     startAnimation("Inner Lock");
    //     startAnimation("Outer Lock");
    //     game_manager->playAudio("Chevron lock", 1);
    // }
    // if (key == "A")
    //     rotateMesh("Ring Rotate", .06, Vector3(0, 0, 1));
    // if (key == "D")
    //     rotateMesh("Ring Rotate", -.06, Vector3(0, 0, 1));
    // if (key == "1") {
    //     if (!stargate_anim->isPlaying()) {
    //         string address = "AJFOER4";
    //         stargate_anim->dialStargate(address);
    //     }
    // }
// }

// void OgreRenderManager::buttonPressed(string button) {
//     if (button == "START")
//         stopRendering();
//     if (button == "A") {
//         startAnimation("Inner Lock");
//         startAnimation("Outer Lock");
//         game_manager->playAudio("Chevron lock", 1);
//     }
// }

// void OgreRenderManager::leftJoystickAxisMoved(float north_south, float east_west) {
//     rotateMesh("Ring Rotate", -.15 * east_west, Vector3(0, 0, 1));
//     if (!game_manager->isAudioPlaying("Ring roll"))
//         game_manager->playAudio("Ring roll", 1, true);
// }

// void OgreRenderManager::leftAxisReleased() {
//     game_manager->pauseAudio("Ring roll");
// }

void OgreRenderManager::rotateMesh(string node, float value, int x, int y, int z) {
    Vector3 newAxis(x, y, z);
    SceneNode* scene_node = scene_manager->getSceneNode(node);
    Quaternion current_quat = scene_node->getOrientation();
    Degree prev_degree;
    Vector3 curr_axis;
    current_quat.ToAngleAxis(prev_degree, curr_axis);

    float curr_deg = prev_degree.valueDegrees() + value;

    if (curr_deg < 0)
        curr_deg += 360.0;
    if (curr_deg > 360)
        curr_deg -= 360.0;

    Degree curr_degree(curr_deg);

    Quaternion update(curr_degree, newAxis);
    scene_node->setOrientation(update);
}

void OgreRenderManager::startAnimation(string animation_name) {
    AnimationState* state = scene_manager->getAnimationState(animation_name);
    if (!state->getEnabled()) {
        animation_playing = true;
        state->setWeight(1);
        state->setTimePosition(0);
        state->setEnabled(true);
    }
}

void OgreRenderManager::loadGUI(string& level, string& scheme, string& font, string& cursor, string& tooltip, string& layout) {
    game_manager->getGUIManager()->loadGUI(level, scheme, font, cursor, tooltip, layout);
}

size_t OgreRenderManager::getRenderWindowHandle()
{
    return window_handle;
}

int OgreRenderManager::getRenderWindowWidth()
{
    return viewport->getActualWidth();
}

int OgreRenderManager::getRenderWindowHeight()
{
    return viewport->getActualHeight();
}

void OgreRenderManager::startRendering() {
    for (int i = 0; i < render_listeners->size(); i++) {
        RenderListener* listener = render_listeners->at(i);
        listener->startRendering();
    }

    root->startRendering();
}

void OgreRenderManager::stopRendering() {
    for (int i = 0; i < render_listeners->size(); i++) {
        RenderListener* listener = render_listeners->at(i);
        listener->stopRendering();
    }
}

RenderWindow* OgreRenderManager::getRenderWindow()
{
    return window;
}

SceneManager* OgreRenderManager::getSceneManager()
{
    return scene_manager;
}

void OgreRenderManager::drawLine(float* from, float* to, float* color, SceneNodeManual* snm) {
    SceneNode* scene_node_manual = snm->scene_node_manual;
    ManualObject* manual_object = (ManualObject*) scene_node_manual->getAttachedObject("Manual_Object");
    manual_object->begin("OgreBulletCollisionsDebugDefault", RenderOperation::OT_LINE_LIST);
    manual_object->position(Vector3(from[0], from[1], from[2]));
    manual_object->colour(ColourValue(color[0], color[1], color[2]));
    manual_object->position(Vector3(to[0], to[1], to[2]));
    manual_object->colour(ColourValue(color[0], color[1], color[2]));
    manual_object->end();
}

SceneNodeMotion* OgreRenderManager::createSceneNodeMotion(string& scene_node_id) {
    SceneNodeMotion* snm = (SceneNodeMotion*) malloc(sizeof(SceneNodeMotion));
    snm->scene_node_motion = scene_manager->getSceneNode(scene_node_id);
    return snm;
}

void OgreRenderManager::destroySceneNodeMotion(SceneNodeMotion* snm) {
    free(snm);
}

SceneNodeManual* OgreRenderManager::createManualObject() {
    SceneNodeManual* scene_node_manual = new SceneNodeManual;

    ManualObject* manual_object = scene_manager->createManualObject("Manual_Object");
    manual_object->setDynamic(true);
    static const char* mat_name = "OgreBulletCollisionsDebugDefault";
    MaterialPtr manual_object_mat = MaterialManager::getSingleton().getDefaultSettings()->clone(mat_name);
    manual_object_mat->setReceiveShadows(false);
    manual_object_mat->getTechnique(0)->setLightingEnabled(false);
    SceneNode* manual_object_node = scene_manager->getRootSceneNode()->createChildSceneNode();
    manual_object_node->attachObject(manual_object);

    scene_node_manual->scene_node_manual = manual_object_node;
    return scene_node_manual;
}

void OgreRenderManager::clearManualObject(SceneNodeManual* snm) {
    SceneNode* scene_node_manual = snm->scene_node_manual;
    ManualObject* manual_object = (ManualObject*) scene_node_manual->getAttachedObject("Manual_Object");
    manual_object->clear();
}

void OgreRenderManager::setPosition(SceneNodeMotion* scene_node_motion, double x, double y, double z) {
    SceneNode* scene_node = scene_node_motion->scene_node_motion;
    scene_node->setPosition(x, y, z);
}

float* OgreRenderManager::getPosition(SceneNodeMotion* scene_node_motion) {
    SceneNode* scene_node = scene_node_motion->scene_node_motion;
    
    Vector3 pos = scene_node->getPosition();
    float* pos_ = new float[3];
    pos_[0] = pos.x;
    pos_[1] = pos.y;
    pos_[2] = pos.z;
    return pos_;
}

void OgreRenderManager::setOrientation(SceneNodeMotion* scene_node_motion, double w, double x, double y, double z) {
    SceneNode* scene_node = scene_node_motion->scene_node_motion;
    scene_node->setOrientation(w, x, y, z);
}

float* OgreRenderManager::getOrientation(SceneNodeMotion* scene_node_motion) {
    SceneNode* scene_node = scene_node_motion->scene_node_motion;
    
    Quaternion q = scene_node->getOrientation();
    Real w = q.w;
    Real x = q.x;
    Real y = q.y;
    Real z = q.z;

    float* rot = new float[4];
    rot[3] = w;
    rot[0] = x;
    rot[1] = y;
    rot[2] = z;
    return rot;
}

PhysicsManager* OgreRenderManager::getPhysicsManager() {
    return physics_manager;
}