#include "Level.h"
#include "LevelResources.h"

Level::Level(const char* level_name, LevelResources* game_resources) {
	name = level_name;
	resources = game_resources;
}

Level::~Level() {
	delete name;
	delete resources;
}

const char* Level::getName() {
	return name;
}

LevelResources* Level::getResources() {
	return resources;
}