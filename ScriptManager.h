#ifndef SCRIPT_MANAGER_H
#define SCRIPT_MANAGER_H

#include <string>
#include <vector>

class GameManager;

class ScriptManager {
	protected:
		GameManager* game_manager;

	public:
		ScriptManager(GameManager* game_manager) { this->game_manager = game_manager; }
		virtual ~ScriptManager() { game_manager = NULL; }

		virtual void setDirectory(std::string directory) = 0;
		virtual void loadScript(std::string path, std::string type) = 0;

		virtual void inputCallback(std::string& function, std::vector<std::string>* inputs) = 0;
		virtual void updateCallback(std::string& file) = 0;
		virtual void eventCallback(std::string& file, std::vector<std::string>* inputs) = 0;
};

#endif