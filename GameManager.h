#if !defined GAME_MANAGER
#define GAME_MANAGER

#include <string>
#include <vector>

#include "GameHeader.h"

using namespace std;

class AudioManager;
class RenderManager;
class ResourceManager;
class LogManager;
class InputManager;
class GUIManager;
class ScriptManager;
class PhysicsManager;
class AudioResource;

//supplies communication between managers
class GameManager
{
   private:
      RenderManager* render_manager;
      ResourceManager* resource_manager;
      LogManager* log_manager;
      InputManager* input_manager;
      AudioManager* audio_manager;
      GUIManager* gui_manager;
      ScriptManager* script_manager;

      GameManager();

   public:
      virtual ~GameManager();
      static GameManager* getGameManager();

      void logProblem(std::string error_msg, std::string file_name, int line_number);
      void logComment(std::string comment_message);

      RenderManager* getRenderManager();
      GUIManager* getGUIManager();
      InputManager* getInputManager();
      AudioManager* getAudioManager();
      ScriptManager* getScriptManager();
      PhysicsManager* getPhysicsManager();

      int getRenderWindowWidth();
      int getRenderWindowHeight();
      size_t getRenderWindowHandle();
	std::string getLoadedLevelName();

      void loadRenderingResources(const char* level_name, std::vector<std::string*>* paths, std::vector<std::string*>* meshes);
      void loadAudioResources(std::vector<AudioResource*>* audio);
      void unloadRenderingResources(const char* file_name);
      void unloadAudioResources(std::vector<AudioResource*>* audio);
};

#endif
